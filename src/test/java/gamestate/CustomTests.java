/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamestate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class CustomTests {

    public CustomTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSpawnSide() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:   7      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#       XXX       #\n"
                + "#          VVV    #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#   x  x  x       #\n"
                + "#                 #\n"
                + "#   x  x  x       #\n"
                + "#                 #\n"
                + "#        ! |      #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   0      #\n"
                + "# Round:   7      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 16);
    }

    @Test
    public void testSpawnSide2() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  10      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#       XXX       #\n"
                + "#          VVV    #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#x  x  x          #\n"
                + "#                 #\n"
                + "#x  x             #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          |      #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "# ---         !-- #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   1      #\n"
                + "# Round:  10      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 16);
    }

    @Test
    public void testSpawnSide3() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  11      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#       XXX       #\n"
                + "#          VVV    #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---   |     --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#x  x  x          #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#x  x             #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---      |  --- #\n"
                + "# ---         !-- #\n"
                + "# ---          -- #\n"
                + "#             AAA #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   1      #\n"
                + "# Round:  11      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 16);
    }

    @Test
    public void testSpawnSide4() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  22      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 1/1   #\n"
                + "###################\n"
                + "#                 #\n"
                + "#      VVV        #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          i      #\n"
                + "#          x  x  x#\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           x     #\n"
                + "#                 #\n"
                + "#             !   #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#  --   |       - #\n"
                + "# ---           - #\n"
                + "# ---           - #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   2      #\n"
                + "# Round:  22      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 16);
    }

    @Test
    public void testSpawnSide5() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  6/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  24      #\n"
                + "# Kills:   1      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#     XXX         #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           |     #\n"
                + "#           x  x  #\n"
                + "#                 #\n"
                + "#        x  x  x  #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#  --           - #\n"
                + "# ---           - #\n"
                + "# ---   |       - #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   3      #\n"
                + "# Round:  24      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  0/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 0);
    }

    @Test
    public void testSpawnSide6() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  25      #\n"
                + "# Kills:   1      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#     XXX         #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "#                 #\n"
                + "#           |     #\n"
                + "#                 #\n"
                + "#          x  x   #\n"
                + "#                 #\n"
                + "#       x  x  x   #\n"
                + "#                 #\n"
                + "# x  x  x  x      #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#  --           - #\n"
                + "# ---           - #\n"
                + "# ---           - #\n"
                + "#       |    AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   3      #\n"
                + "# Round:  25      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 0);
    }

    @Test
    public void testSpawnSide7() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  31      #\n"
                + "# Kills:   1      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 1/1   #\n"
                + "###################\n"
                + "#     XXX   |     #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# --- i        -- #\n"
                + "#                 #\n"
                + "#     |           #\n"
                + "#                 #\n"
                + "#    x  x         #\n"
                + "#                 #\n"
                + "# x  x  x         #\n"
                + "#                 #\n"
                + "#       x  x  x  x#\n"
                + "#                 #\n"
                + "#      |          #\n"
                + "#                 #\n"
                + "#            !    #\n"
                + "#                 #\n"
                + "#  --           - #\n"
                + "# ---   |    !  - #\n"
                + "# ---           - #\n"
                + "#           AAA   #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 2/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   3      #\n"
                + "# Round:  31      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  1/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 0);
    }

    @Test
    public void testSpawnSide8() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  0/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  36      #\n"
                + "# Kills:   1      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 1/1   #\n"
                + "###################\n"
                + "#     XXX         #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "#                 #\n"
                + "#      i  |       #\n"
                + "#      x  x       #\n"
                + "#                 #\n"
                + "#   x  x  x       #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#   x  x     x    #\n"
                + "#      |     !    #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#  --           - #\n"
                + "# ---  |     !  - #\n"
                + "# ---           - #\n"
                + "#           AAA   #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 2/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   4      #\n"
                + "# Round:  36      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  0/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        assertTrue("Spawn side should be 0.", g.getPlayer1SpawnSide() == 0);
    }

    @Test
    public void testAlienValue() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  5   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 12/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  48      #\n"
                + "# Kills:   5      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#    MMMXXX       #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#  --         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x          #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#x  x  x  x       #\n"
                + "#                 #\n"
                + "#      x  x       #\n"
                + "#                 #\n"
                + "#            x    #\n"
                + "#                 #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "#             AAA #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   5      #\n"
                + "# Round:  48      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 24/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        String state2 = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  5   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 12/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  48      #\n"
                + "# Kills:   5      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#    MMMXXX       #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#  --         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x          #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#   x  x  x  x  x #\n"
                + "#                 #\n"
                + "#            x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   5      #\n"
                + "# Round:  48      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 24/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        SimpleState g = new SimpleState(new ByteArrayInputStream(state.getBytes()));

        SimpleState g2 = new SimpleState(new ByteArrayInputStream(state2.getBytes()));
        System.out.println(g.getAlienValue(g));
        System.out.println(g2.getAlienValue(g2));
        assertTrue(String.format("Worse to be in state 1 rather than state 2 %.2f vs %.2f", g.getAlienValue(g), g2.getAlienValue(g2)), g.getAlienValue(g) < g2.getAlienValue(g2));
    }

    @Test
    public void testBinary() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  10      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#       XXX       #\n"
                + "#          VVV    #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#x  x  x          #\n"
                + "#                 #\n"
                + "#x  x             #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          |      #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "# ---         !-- #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   1      #\n"
                + "# Round:  10      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        int rows[] = g.getRowSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        int cols[] = g.getColSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        assertTrue("ROws 0 should be 2.", Arrays.equals(rows, new int[]{2, 0, 0, 0, 0}));
        assertTrue("Cols 0 should be 2.", Arrays.equals(cols, new int[]{1, 1, 0, 0, 0}));
    }

    @Test
    public void testBinary2() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: 1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  10      #\n"
                + "# Kills:   0      #\n"
                + "# Lives: 1        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#       XXX       #\n"
                + "#          VVV    #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#x  x  x          #\n"
                + "#                 #\n"
                + "#x  x             #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          |      #\n"
                + "# ---         --- #\n"
                + "# ---          -- #\n"
                + "# ---         !-- #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   1      #\n"
                + "# Round:  10      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  4/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        int rows[] = g.getRowSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        int cols[] = g.getColSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        assertTrue("ROws 0 should be 2.", Arrays.equals(rows, new int[]{2, 0, 0, 0, 0}));
        assertTrue("Cols 0 should be 2.", Arrays.equals(cols, new int[]{0, 0, 0, 1, 1}));
    }

    @Test
    public void testBinary3() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  5   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy: 12/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  48      #\n"
                + "# Kills:   5      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#    MMMXXX       #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#  --         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x          #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#   x  x  x  x  x #\n"
                + "#                 #\n"
                + "#            x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   5      #\n"
                + "# Round:  48      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 24/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        int rows[] = g.getRowSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        int cols[] = g.getColSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        assertTrue("ROws 0 should be 2.", Arrays.equals(rows, new int[]{5, 2, 0, 0, 0}));
        assertTrue("Cols 0 should be 2.", Arrays.equals(cols, new int[]{1, 1, 1, 2, 2}));
    }

    @Test
    public void testBinary4() throws IOException {
        String state = "###################\n"
                + "# Alien MiniMax   #\n"
                + "# Wave Size:  5   #\n"
                + "# Delta X: 1     #\n"
                + "# Energy: 12/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  48      #\n"
                + "# Kills:   5      #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#    MMMXXX       #\n"
                + "#     VVV         #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#  --         --- #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x          #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#   x  x  x  x  x #\n"
                + "#                 #\n"
                + "#            x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "# ---             #\n"
                + "#            AAA  #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   5      #\n"
                + "# Round:  48      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy: 24/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Right MiniMax   #\n"
                + "###################";

        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        int rows[] = g.getRowSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        int cols[] = g.getColSizes(g.getFlippedXOffset(), g.getYOffset(), g.getFlip());
        assertTrue("ROws 0 should be 2.", Arrays.equals(rows, new int[]{5, 2, 0, 0, 0}));
        assertTrue("Cols 0 should be 2.", Arrays.equals(cols, new int[]{2, 2, 1, 1, 1}));
    }

    @Test
    public void testBinary5() throws IOException {
        String state = "###################\n"
                + "# Alien Expectimax #\n"
                + "# Wave Size:  5   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  0/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round: 138      #\n"
                + "# Kills:  25      #\n"
                + "# Lives: 997        #\n"
                + "# Missiles: 2/2   #\n"
                + "###################\n"
                + "#        XXXMMM   #\n"
                + "#          VVV    #\n"
                + "# ---       i  -  #\n"
                + "# ---             #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#            i    #\n"
                + "#                x#\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# x  x  x  x  x   #\n"
                + "#                 #\n"
                + "# x     x  x  x   #\n"
                + "#                 #\n"
                + "#          x  x   #\n"
                + "#                 #\n"
                + "# x  x  x  x  x   #\n"
                + "# |-           -- #\n"
                + "# !            -- #\n"
                + "#AAA              #\n"
                + "# MMM             #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 0        #\n"
                + "# Kills:  25      #\n"
                + "# Round: 138      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  6/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Right Expectimax #\n"
                + "###################";
        SimpleState g = new SimpleState(new ByteArrayInputStream(state.getBytes()));
        g.getExtremeWeights();

    }

    @Test
    public void dangerousBulletTest() throws IOException, InterruptedException {
        String state = "###################\n"
                + "# Right Expectimax #\n"
                + "# Wave Size:  5   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  59      #\n"
                + "# Kills:   9      #\n"
                + "# Lives: 998        #\n"
                + "# Missiles: 2/2   #\n"
                + "###################\n"
                + "#         XXXMMM  #\n"
                + "#     VVV         #\n"
                + "# ---          -- #\n"
                + "# ---  i       -- #\n"
                + "#  -   i       -  #\n"
                + "#                 #\n"
                + "#     x           #\n"
                + "#                 #\n"
                + "#     x  x        #\n"
                + "#                 #\n"
                + "#     x  x  x  x  #\n"
                + "#                 #\n"
                + "#   x  x  x  x  x #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#   -        |  - #\n"
                + "#  --           - #\n"
                + "# ---           - #\n"
                + "#              AAA#\n"
                + "#         XXXMMM  #\n"
                + "###################\n"
                + "# Missiles: 0/2   #\n"
                + "# Lives: 0        #\n"
                + "# Kills:  10      #\n"
                + "# Round:  59      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  5/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  5   #\n"
                + "# Right Expectimax #\n"
                + "###################";
        SimpleState g = new SimpleState(new ByteArrayInputStream(state.getBytes()));
        double killValue = 5 * Math.exp(-Math.pow(200 - 150, 2) / 4000.0) / Math.sqrt(40 * Math.PI) + 1;
        g.getStateValue();

    }

    @Test
    public void killWaveTest() throws IOException, InterruptedException {
        String state = "###################\n"
                + "# No Alien Expectimax #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  3/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  21      #\n"
                + "# Kills:   2      #\n"
                + "# Lives: 999        #\n"
                + "# Missiles: 2/2   #\n"
                + "###################\n"
                + "#           MMM   #\n"
                + "#         VVV     #\n"
                + "# ---      i   -- #\n"
                + "# ---          -- #\n"
                + "#  --          -- #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           i     #\n"
                + "#          x      #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          x      #\n"
                + "#           !     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       |         #\n"
                + "# ---          -- #\n"
                + "# ---          -- #\n"
                + "# ---          -- #\n"
                + "#          AAA    #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   2      #\n"
                + "# Round:  21      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  3/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right Expectimax #\n"
                + "###################";
        String state2 = "###################\n"
                + "# No Alien Expectimax #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  3/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  21      #\n"
                + "# Kills:   2      #\n"
                + "# Lives: 999        #\n"
                + "# Missiles: 2/2   #\n"
                + "###################\n"
                + "#           MMM   #\n"
                + "#         VVV     #\n"
                + "# ---      i   -- #\n"
                + "# ---          -- #\n"
                + "#  --          -- #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           i     #\n"
                + "#          x      #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          x      #\n"
                + "#           !     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       |         #\n"
                + "# ---          -- #\n"
                + "# ---          -- #\n"
                + "# ---        ! -- #\n"
                + "#           AAA   #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 2/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   2      #\n"
                + "# Round:  21      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  3/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right Expectimax #\n"
                + "###################";
        SimpleState g = new SimpleState(new ByteArrayInputStream(state.getBytes()));
        SimpleState g2 = new SimpleState(new ByteArrayInputStream(state2.getBytes()));
        assertTrue("Doesn't kill whole wave.", g2.getStateValue() > g.getStateValue());

    }

    @Test
    public void killWaveTest2() throws IOException, InterruptedException {
        String state = "###################\n"
                + "# No Alien Expectimax #\n"
                + "# Wave Size:  4   #\n"
                + "# Delta X: -1     #\n"
                + "# Energy:  2/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  92      #\n"
                + "# Kills:  15      #\n"
                + "# Lives: 999        #\n"
                + "# Missiles: 0/2   #\n"
                + "###################\n"
                + "#           MMM   #\n"
                + "#           VVV   #\n"
                + "# ---          -- #\n"
                + "# ---          -- #\n"
                + "#   -        |  - #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#             x   #\n"
                + "#                 #\n"
                + "#          x      #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x  x     x #\n"
                + "#                 #\n"
                + "#                |#\n"
                + "#                 #\n"
                + "#                 #\n"
                + "# -        !      #\n"
                + "# - -             #\n"
                + "# ---             #\n"
                + "#         AAA     #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 1/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:  15      #\n"
                + "# Round:  92      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  2/6    #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  4   #\n"
                + "# Right Expectimax #\n"
                + "###################";
        String state2 = "###################\n"
                + "# No Alien Expectimax #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy:  3/6    #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  21      #\n"
                + "# Kills:   2      #\n"
                + "# Lives: 999        #\n"
                + "# Missiles: 2/2   #\n"
                + "###################\n"
                + "#           MMM   #\n"
                + "#         VVV     #\n"
                + "# ---      i   -- #\n"
                + "# ---          -- #\n"
                + "#  --          -- #\n"
                + "#       |         #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#           i     #\n"
                + "#          x      #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#          x      #\n"
                + "#           !     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#       |         #\n"
                + "# ---          -- #\n"
                + "# ---          -- #\n"
                + "# ---        ! -- #\n"
                + "#           AAA   #\n"
                + "#            MMM  #\n"
                + "###################\n"
                + "# Missiles: 2/2   #\n"
                + "# Lives: 1        #\n"
                + "# Kills:   2      #\n"
                + "# Round:  21      #\n"
                + "# Respawn: -1     #\n"
                + "# Energy:  3/6    #\n"
                + "# Delta X:  1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Right Expectimax #\n"
                + "###################";
        SimpleState g = new SimpleState(new ByteArrayInputStream(state.getBytes()));
        g.getStateValue();
        SimpleState g2 = new SimpleState(new ByteArrayInputStream(state2.getBytes()));
        assertTrue("Doesn't kill whole wave.", g2.getStateValue() > g.getStateValue());

    }

    @Test
    public void defend() throws IOException, InterruptedException {
        String state = "###################\n" +
"# Right Expectimax #\n" +
"# Wave Size:  5   #\n" +
"# Delta X:  1     #\n" +
"# Energy:  4/6    #\n" +
"# Respawn: -1     #\n" +
"# Round: 160      #\n" +
"# Kills:  33      #\n" +
"# Lives: 998        #\n" +
"# Missiles: 2/2   #\n" +
"###################\n" +
"#         XXXMMM  #\n" +
"#          VVV    #\n" +
"#                 #\n" +
"#                 #\n" +
"#   |             #\n" +
"#          i      #\n" +
"#   x     x       #\n" +
"#                 #\n" +
"#   x  x  x       #\n" +
"#             i   #\n" +
"#   x  x  x  x  x #\n" +
"#                 #\n" +
"#     x  x  x  x  #\n" +
"#                 #\n" +
"#     x  x  x  x  #\n" +
"#                 #\n" +
"#     x !   x     #\n" +
"#                 #\n" +
"#     x         - #\n" +
"#                 #\n" +
"#     x!          #\n" +
"#     AAA         #\n" +
"#         XXXMMM  #\n" +
"###################\n" +
"# Missiles: 2/2   #\n" +
"# Lives: 0        #\n" +
"# Kills:  36      #\n" +
"# Round: 160      #\n" +
"# Respawn: -1     #\n" +
"# Energy:  4/6    #\n" +
"# Delta X:  1     #\n" +
"# Wave Size:  5   #\n" +
"# Right Expectimax #\n" +
"###################";
        String state2 = "###################\n" +
"# Right Expectimax #\n" +
"# Wave Size:  5   #\n" +
"# Delta X:  1     #\n" +
"# Energy:  4/6    #\n" +
"# Respawn: -1     #\n" +
"# Round: 160      #\n" +
"# Kills:  33      #\n" +
"# Lives: 998        #\n" +
"# Missiles: 2/2   #\n" +
"###################\n" +
"#         XXXMMM  #\n" +
"#          VVV    #\n" +
"#                 #\n" +
"#                 #\n" +
"#   |             #\n" +
"#          i      #\n" +
"#   x     x       #\n" +
"#                 #\n" +
"#   x  x  x       #\n" +
"#             i   #\n" +
"#   x  x  x  x  x #\n" +
"#                 #\n" +
"#     x  x  x  x  #\n" +
"#                 #\n" +
"#     x  x  x  x  #\n" +
"#                 #\n" +
"#     x !   x     #\n" +
"#                 #\n" +
"#     x         - #\n" +
"#                 #\n" +
"#     x           #\n" +
"#     AAA         #\n" +
"#         XXXMMM  #\n" +
"###################\n" +
"# Missiles: 1/2   #\n" +
"# Lives: 0        #\n" +
"# Kills:  36      #\n" +
"# Round: 160      #\n" +
"# Respawn: -1     #\n" +
"# Energy:  4/6    #\n" +
"# Delta X:  1     #\n" +
"# Wave Size:  5   #\n" +
"# Right Expectimax #\n" +
"###################";
        SimpleState g = new SimpleState(new ByteArrayInputStream(state.getBytes()));
        g.getStateValue();
        SimpleState g2 = new SimpleState(new ByteArrayInputStream(state2.getBytes()));
        assertTrue("Doesn't kill whole wave.", g.getStateValue() > g2.getStateValue());
    }

}
