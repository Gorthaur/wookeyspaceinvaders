/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public class TempTests {

    public TempTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    public static void main(String args[]) throws IOException {
        TempTests t = new TempTests();
        t.testGameState();
    }

    @Test
    public void testGameState() throws IOException {
        String state = "###################\n"
                + "# Player 2        #\n"
                + "# Wave Size:  3   #\n"
                + "# Delta X:  1     #\n"
                + "# Energy: 8/6     #\n"
                + "# Respawn: -1     #\n"
                + "# Round:  26      #\n"
                + "# Kills: 0        #\n"
                + "# Lives: 0        #\n"
                + "# Missiles: 0/1   #\n"
                + "###################\n"
                + "#          |      #\n"
                + "#      VVV        #\n"
                + "# ---   -     --- #\n"
                + "# ---   - -   --- #\n"
                + "# ---    --   --- #\n"
                + "#           |     #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#      x  x  x    #\n"
                + "#                 #\n"
                + "#      x  x  x    #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#               x #\n"
                + "#         !       #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#                 #\n"
                + "#  --         --- #\n"
                + "# ---         --- #\n"
                + "# ---         --- #\n"
                + "#     AAA|        #\n"
                + "#                 #\n"
                + "###################\n"
                + "# Missiles: 1/1   #\n"
                + "# Lives: 1        #\n"
                + "# Kills: 2        #\n"
                + "# Round:  26      #\n"
                + "# Respawn: 0      #\n"
                + "# Energy: 2/6     #\n"
                + "# Delta X: -1     #\n"
                + "# Wave Size:  3   #\n"
                + "# Player 1        #\n"
                + "###################\n";
        
        GameState g = new GameState(new ByteArrayInputStream(state.getBytes()));
        System.out.print(g.getMapString());
    }
}
