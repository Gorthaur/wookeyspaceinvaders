package za.ac.wits.spaceinvaders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import za.ac.wits.spaceinvaders.wookey.Agent;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.agents.MaxKillExpectimax;
import za.ac.wits.spaceinvaders.wookey.agents.MoveRightExpectimax;
import za.ac.wits.witstestablespaceinvaders.Move;

public class Main {

    public static void main(String args[]) throws FileNotFoundException, IOException {
        File input = new File(args[0] + "/map-detailed.txt");
        File output = new File(args[0] + "/move.txt");
        try {
            long startTime = System.currentTimeMillis();
            Agent agent = new MoveRightExpectimax();
            //Agent agent = new MaxKillExpectimax();

            FileInputStream in = new FileInputStream(input);
            GameState g = agent.getGameState(in);
            writeSavedMove(g.getRound(), output);
            List<Move> moves = agent.getMoves(g);
            in.close();
            writeMove(moves.get(0), output);
            saveFutureMoves(g.getRound(), moves);
            System.out.println("Time taken for agent " + agent.getDescription() + " " + (System.currentTimeMillis() - startTime));
        } catch (Exception e) {
            writeMove(Move.NONE, output);
            e.printStackTrace();
        }
    }

    public static void writeMove(Move move, File outputFile) throws IOException {
        try (FileWriter writer = new FileWriter(outputFile)) {
            writer.write(move.toString() + System.lineSeparator());
            writer.close();
        }
    }

    private static void writeSavedMove(int round, File outputFile) throws IOException {
        File input = new File("future-moves.txt");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(input)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                String split[] = line.split(":");
                if (Integer.parseInt(split[0]) == round) {
                    Move m = Move.valueOf(line);
                    writeMove(m, outputFile);
                    return;
                }
            }
        } catch (IllegalArgumentException | FileNotFoundException ex) {
            writeMove(Move.NONE, outputFile);
        }
    }

    private static void saveFutureMoves(int round, List<Move> moves) throws IOException {
        File output = new File("future-moves.txt");
        Iterator<Move> it = moves.iterator();
        if (it.hasNext()) {
            it.next();
        }
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            Move m = it.next();
            round++;
            sb.append(round).append(':').append(m.name()).append('\n');
        }
        try (FileWriter fw = new FileWriter(output)) {
            fw.write(sb.toString());
        }
    }
}
