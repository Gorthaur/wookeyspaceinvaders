package za.ac.wits.spaceinvaders.wookey.agents;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveLeftOpening;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class MoveLeftMiniMax extends SimpleMiniMax {

    public MoveLeftMiniMax() {
        this.setOpening(new FireMoveLeftOpening());
    }

    @Override
    public String getDescription() {
        return "Left MiniMax";
    }

}
