/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import java.util.ArrayList;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;
import za.ac.wits.witstestablespaceinvaders.SpaceInvaderObject;

/**
 *
 * @author Dean
 */
public class BackRowFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 320;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        
        if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        boolean record = false;
        if (g.player1.maxCentreDistance <= 4) {
            record = true;
        }
        int xOffset = g.getFlippedXOffset();
        boolean flip = g.getFlip();

        int yOffset = g.getYOffset();

        int y = yOffset + 11;

        int count = 0;
        for (int i = 0; i < 5; i++) {
            int x = xOffset + i * 3;
            if (flip) {
                x = 16 - x;
            }
            if (g.getSpaceInvaderObjectAt(new Point2D(x, y)) == SpaceInvaderObject.ALIEN) {
                count += Math.pow(2, i);
            }
        }
        double vals[] = new double[320];
        if (record) {
            int c = 0;
            for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
                for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                        for (int i = 0; i < 32; i++) {
                            if (xoffsets == xOffset && yOffset == yoffsets && count == i) {
                                vals[c] = 1.0;
                            }
                            c++;
                        }
                    
                }
            }
        }
        return vals;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        if (g.player1.maxCentreDistance > 4) {
            return new int[]{};
        }
        int xOffset = g.getFlippedXOffset();
        boolean flip = g.getFlip();

        int yOffset = g.getYOffset();

        int y = yOffset + 11;

        int count = 0;
        for (int i = 0; i < 5; i++) {
            int x = xOffset + i * 3;
            if (flip) {
                x = 16 - x;
            }
            if (g.getSpaceInvaderObjectAt(new Point2D(x, y)) == SpaceInvaderObject.ALIEN) {
                count += Math.pow(2, i);
            }
        }
        return new int[]{getIndex(new int[]{5,2,32}, new int[]{xOffset, yOffset, count})};
    }

}
