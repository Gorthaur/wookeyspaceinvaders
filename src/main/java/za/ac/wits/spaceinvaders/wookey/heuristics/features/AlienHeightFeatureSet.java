package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class AlienHeightFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 7;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        double vals[] = new double[7];
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int height = (g.getPlayer1AlienWaveHeight()-1)/2;
        if (height <0) {
            height = 0;
        }
        else {
            height = height + 1;
        }
        for (int i = 0; i < 7; i++) {
            if (i == height) {
                vals[i] =1;
            }
        }
        return vals;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int height = (g.getPlayer1AlienWaveHeight()-1)/2;
        if (height <0) {
            height = 0;
        }
        else {
            height = height + 1;
        }
        if (height < 7) {
            return new int[]{height};
        }
        else {
            return new int[]{0};
        }
    }
    
}
