package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class LowHeightFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 5;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        double vals[] = new double[5];
        int width = (g.getPlayer1AlienWaveWidth() + 2) / 3;
        width = width - 1;
        if (g.getPlayer1AlienWaveHeight() < 4) {
            for (int i = 0; i < 5; i++) {
                if (i == width) {
                    vals[i] = 1;
                }
            }
        }
        return vals;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        if (g.getPlayer1AlienWaveHeight() < 4) {
            int width = (g.getPlayer1AlienWaveWidth() + 2) / 3;
            width = width - 1;
            if (width >= 0 && width < 5) {
                return new int[]{width};
            }
        }
        return new int[]{};
    }

}
