
package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public class ConstantFeature extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 1;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        return new double[]{1};
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
        return new int[]{0};
    }
    
}
