package za.ac.wits.spaceinvaders.wookey.agents;

import java.util.ArrayList;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class MoveValue implements Comparable<MoveValue> {

    public double value;
    public Move move;
    public ArrayList<AlienMove> children = new ArrayList<>();

    public MoveValue(double value, Move move) {
        this.value = value;
        this.move = move;
    }

    @Override
    public String toString() {
        return "MoveValue{" + "value=" + value + ", move=" + move + ' ' + children.get(0) +'}';
    }

    @Override
    public int compareTo(MoveValue o) {
        if (value > o.value) {
            return -1;
        }
        else if (value < o.value) {
            return 1;
        }
        else {
            return 0;
        }
    }

}
