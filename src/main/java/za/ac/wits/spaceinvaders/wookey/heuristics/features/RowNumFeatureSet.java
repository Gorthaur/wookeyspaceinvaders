package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import java.util.ArrayList;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class RowNumFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 300;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        double values[] = new double[300];
        if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int x = g.getFlippedXOffset();
        boolean flip = g.getFlip();

        int yOffset = g.getYOffset();

        int numInRows[] = g.getRowSizes(x, yOffset, flip);
        int count = 0;
        for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
            for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                for (int row = 0; row < 5; row++) {
                    for (int numInRow = 0; numInRow < 6; numInRow++) {
                        if (xoffsets == x && yOffset == yoffsets && numInRows[row] == numInRow) {
                            values[count] = 1.0;
                        } else {
                            values[count] = 0;
                        }
                        count++;
                    }
                }
            }
        }
        return values;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int maxValues[] = new int[]{5,2,5,6};
        int active[] = new int[5];
        int x = g.getFlippedXOffset();
        boolean flip = g.getFlip();

        int yOffset = g.getYOffset();

        int numInRows[] = g.getRowSizes(x, yOffset, flip);
        for (int i = 0; i < numInRows.length; i++) {
            active[i] = getIndex(maxValues, new int[]{x, yOffset, i, numInRows[i]});
        }
        return active;
    }

}
