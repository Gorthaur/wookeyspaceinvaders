
package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public class ShipPositionFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 5;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        boolean flip = g.getFlip();
        int x = g.getPlayer1X();
        if (flip) {
            x = 16-x;
        }
        double vals[] = new double[5];
        if (x < 4) {
            vals[0] =1;
        }
        if (x < 7 && x >=4) {
            vals[1] = 1;
        }
        if (x >=7 && x <= 9) {
            vals[2] = 1;
        }
        if (x > 9 && x <= 12) {
            vals[3] = 1;
        }
        if (x > 12) {
            vals[4] = 1;
        }
        return vals;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                boolean flip = g.getFlip();
        int x = g.getPlayer1X();
        if (flip) {
            x = 16-x;
        }
        if (x < 4) {
            return new int[]{0};
        }
        if (x < 7 && x >=4) {
            return new int[]{1};
        }
        if (x >=7 && x <= 9) {
            return new int[]{2};
        }
        if (x > 9 && x <= 12) {
            return new int[]{3};
        }
        if (x > 12) {
            return new int[]{4};
        }
        else {
            return new int[]{};
        }
    }
    
}
