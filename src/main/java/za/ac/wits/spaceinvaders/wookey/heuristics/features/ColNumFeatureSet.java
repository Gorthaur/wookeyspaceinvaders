package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import java.util.ArrayList;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class ColNumFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 300;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        double values[] = new double[300];
        if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int x = g.getFlippedXOffset();
        boolean flip = g.getFlip();

        int yOffset = g.getYOffset();

        int numInCols[] = g.getColSizes(x, yOffset, flip);
        int count = 0;
        for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
            for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                for (int col = 0; col < 5; col++) {
                    for (int numInCol = 0; numInCol < 6; numInCol++) {
                        if (xoffsets == x && yOffset == yoffsets && numInCols[col] == numInCol) {
                           values[count] = 1;
                        } else {
                            values[count] = 0;
                        }
                        count++;
                    }
                }
            }
        }
        return values;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int maxValues[] = new int[]{5,2,5,6};
        int active[] = new int[5];
        int x = g.getFlippedXOffset();
        boolean flip = g.getFlip();

        int yOffset = g.getYOffset();

        int numInCols[] = g.getColSizes(x, yOffset, flip);
        for (int i = 0; i < numInCols.length; i++) {
            active[i] = getIndex(maxValues, new int[]{x, yOffset, i, numInCols[i]});
        }
        return active;
    }

}
