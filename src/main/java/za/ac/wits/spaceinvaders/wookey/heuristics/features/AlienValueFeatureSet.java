package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class AlienValueFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 3;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        if (g instanceof SimpleState) {
            SimpleState s = (SimpleState) g;
            return new double[]{s.getCurrentWeight()/200.0, s.getFutureWeight()/200.0, s.distanceFromSpawnSide/16.0};
        }
        return new double[]{0, 0,0};
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
        return new int[]{0, 1,2};
    }

    @Override
    public double[] getValues(GameState g) {
        return getFeatureValues(g);
    }
    
    

}
