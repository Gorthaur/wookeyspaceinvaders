package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import java.awt.Point;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Dean
 */
public class SecondaryFunctionApproximator extends LinearFunctionApproximator {

    static final HashMap<List<Integer>, Double> featureValues = new HashMap<>();
    static FeatureSet[] featureSets = new FeatureSet[]{new RowNumFeatureSet(), new ColNumFeatureSet(), new ConstantFeature(), new ShipPositionFeatureSet(), new LowHeightFeatureSet(), new AlienWidthFeatureSet(), new AlienHeightFeatureSet(), new WidthHeightFeatureSet(), new OffsetFeatureSet(), new AlienMissileFactoryFeature(), new OnSpawnSideFeature(), new AlienValueFeatureSet(), new SpawnColumnFeatureSet(), new BackRowFeatureSet(), new BulletFeatureSet()};

    static {
    }

    @Override
    public FeatureSet[] getFeatureSets() {
        return featureSets;
    }

    @Override
    public HashMap<List<Integer>, Double> getValueMap() {
        return featureValues;
    }
}
