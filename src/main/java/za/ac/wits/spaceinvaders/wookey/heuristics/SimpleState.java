package za.ac.wits.spaceinvaders.wookey.heuristics;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.SpaceInvaderObject;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

/**
 *
 * @author Dean
 */
public class SimpleState extends GameState implements TestableGameState, Serializable {

    //public static double taskChooseTimes[] = new double[20];
    //static LinearFunctionApproximator LFA = new PrimaryFunctionApproximator();
    //static LinearFunctionApproximator NOFACTORYLFA = new SecondaryFunctionApproximator();
    GameState futureState = null;
    public static ExecutorService executor = Executors.newFixedThreadPool(10,
            new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = Executors.defaultThreadFactory().newThread(r);
                    t.setDaemon(true);
                    return t;
                }
            });

    public double missileMisses = 0;
    public double futureBulletMisses = 0; //bullets that are guaranteed to miss
    double futureAlienWeights = 0;
    double alienWeights = 0;
    //public double futureAlienValue = 0;
    public double currentAlienValue = 0;
    public double dangerousBullets = 0; //bullets heading for ship/factory that won't be hit by missiles
    public double superDangerousBullets = 0;
    public double futureDangerousBullets = 0; //bullets that can be shot at the ship/factory that can't be easily intercepted
    double unusedBullets = 0; //want to minimise unused bullets to maximise kills. At the same time, a tradeoff between keeping bullets to defend the factory must be maintained
    double squaredDistanceFromSpawnSide = 0;
    double squaredDistanceFromNonSpawnSide = 0;
    double squaredDistanceFromMissileFactory = 0;
    public double distanceFromSpawnSide = 0;
    public double lives = 0;
    double missileFactory = 0;
    double alienFactory = 0;
    public double futureKills = 0;
    double shipX;
    double timeTillRespawn;

    boolean valueCalculated = false;
    double value = 0;
    double squaredDistanceFromAlienFactory;
    int timeTillFreeMissile;
    private int numAliens;
    private int width;
    private int height;
    public double distanceFromMissileFactory;
    private boolean initialised;
    private double basicValue = Double.NaN;
    public SimpleState next;
    public double extremeWeights;
    public int distanceFromOptPosition;
    public double futureExtremeWeights;

    private double killValue;
    private int aliens;
    private ArrayList<GameState> allStates;

    public SimpleState(InputStream in) throws IOException {
        super(in);
    }

    public SimpleState(InputStream in, boolean player2Spawn) throws IOException {
        super(in, player2Spawn);
    }

    protected SimpleState(GameState previous, Move player1Move, Move player2Move) {
        this(previous, player1Move, player2Move, new Point2D[0]);
    }

    protected SimpleState(GameState previous, Move player1Move, Move player2Move, Point2D[] bullets) {
        super(previous, player1Move, player2Move, bullets);
    }

    protected SimpleState(GameState previous, Move player1Move, Move player2Move, List<Point2D> bullets) {
        super(previous, player1Move, player2Move, bullets);
    }

    @Override
    public SimpleState getNextState(Move move, Move move1) {
        SimpleState g = new SimpleState(this, move, move1);
        return g;
    }

    @Override
    public GameState getNextState(Move move, Move move1, Point2D... pds) {
        return new SimpleState(this, move, move1, pds);
    }

    public SimpleState getNextState(Move move, Move move1, List<Point2D> pds) {
        return new SimpleState(this, move, move1, pds);
    }

    @Override
    public double getStateValue() throws InterruptedException {
        if (valueCalculated) {
            return value;
        } else {
            initialiseStateVariables();
            value = calculateStateValue();
            valueCalculated = true;
            return value;
        }
    }

    public void initialiseStateVariables() {
        if (initialised) {
            return;
        }
        initialised = true;
        unusedBullets = (getPlayer1MissileLimit() - getPlayer1Missiles().size());
        lives = getPlayer1Lives();
        missileFactory = this.player1MissileFactory() ? 1 : 0;
        alienFactory = this.player1AlienFactory() ? 1 : 0;
        squaredDistanceFromSpawnSide = Math.pow(this.getPlayer1SpawnSide() - this.getPlayer1X(), 2);
        squaredDistanceFromNonSpawnSide = Math.pow((16 - this.getPlayer1SpawnSide()) - this.getPlayer1X(), 2);
        distanceFromSpawnSide = Math.abs(Math.abs(this.getPlayer1SpawnSide() - 3) - this.getPlayer1X());
        distanceFromOptPosition = Math.abs(Math.abs(Math.abs(16 - this.getPlayer1SpawnSide()) - 3) - this.getPlayer1X());
        if (this.player1MissileFactory() && !this.player1AlienFactory()) {
            distanceFromMissileFactory = Math.abs(this.getPlayer1X() - this.getPlayer1MissileFactory());
        }
        if (!this.player1MissileFactory() && !this.player1AlienFactory()) {
            distanceFromMissileFactory = Math.abs(this.getPlayer1SpawnSide() - this.getPlayer1X());
        } else if (this.player1AlienFactory() && this.player1MissileFactory()) {
            distanceFromMissileFactory = Math.abs(this.getPlayer1X() - (this.getPlayer1MissileFactory() + this.getPlayer1AlienFactory()) / 2.0);
        } else if (this.player1AlienFactory()) {
            distanceFromMissileFactory = Math.abs(this.getPlayer1X() - this.getPlayer1AlienFactory());
        }
        squaredDistanceFromMissileFactory = this.player1MissileFactory() ? Math.pow(this.getPlayer1MissileFactory() - this.getPlayer1X(), 2) : 0;
        squaredDistanceFromAlienFactory = this.player1AlienFactory() ? Math.pow(this.getPlayer1AlienFactory() - this.getPlayer1X(), 2) : 0;
        currentAlienValue = getAlienValue(this);

        dangerousBullets += getBulletValue(this, this.getPlayer2Missiles());
        dangerousBullets += getBulletValue(this, this.getPlayer1AlienMissiles());
        Iterator<Point2D> missileIterator = this.getPlayer1Missiles().iterator();
        while (missileIterator.hasNext()) {
            Point2D missile = missileIterator.next();
            if (missile.getY() < 11) {
                //missileIterator.remove();
                missileMisses++;
            }
        }
        timeTillFreeMissile = Integer.MAX_VALUE;
        if (this.getPlayer1Missiles().size() < this.getPlayer1MissileLimit()) {
            timeTillFreeMissile = 0;
        }
        GameState curr = this;
        this.allStates = new ArrayList<>();
        allStates.add(this);
        boolean alienFactory = false;
        if (this.player2AlienFactory()) {
            alienFactory = true;
        }
        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                break;
            }
            GameState nextState = curr.getNextState(Move.NONE, Move.NONE);
            allStates.add(nextState);
            if (nextState.getPlayer1Missiles().size() < nextState.getPlayer1MissileLimit()) {
                timeTillFreeMissile = Math.min(timeTillFreeMissile, i + 1);
            }
            List<Point2D> bullets = getAllBullets(curr);
            boolean allMiss = true;
            if (aliensGuaranteedToFire(bullets, nextState)) {
                for (Point2D b : bullets) {
                    if (this.player1MissileFactory() && Math.abs(b.getX() - getPlayer1MissileFactory()) <= 1) {
                        allMiss = false;
                    }
                }
                if (allMiss) {
                    futureBulletMisses++;
                }
                nextState.setPlayer1ShotEnergy(nextState.getPlayer1AlienShotEnergy() - 6);
            }

            missileIterator = nextState.getPlayer1Missiles().iterator();
            while (missileIterator.hasNext()) {
                Point2D missile = missileIterator.next();
                if (missile.getY() == 11) {
                    //missileIterator.remove();
                    missileMisses++;
                }
            }
            curr = nextState;
        }
        int count = 0;
        for (GameState g : allStates) {
            List<Point2D> bullets = getAllBullets(g);
            for (Point2D b : bullets) {
                if (this.player1MissileFactory() && Math.abs(b.getX() - getPlayer1MissileFactory()) <= 1) {
                    if (!canIntercept(b, g, timeTillFreeMissile - count)) {
                        superDangerousBullets++;
                    } else {
                        futureDangerousBullets++;
                    }
                }
                if (this.player1AlienFactory() && Math.abs(b.getX() - getPlayer1AlienFactory()) <= 1) {
                    if (!canIntercept(b, g, timeTillFreeMissile - count)) {
                        superDangerousBullets++;
                    } else {
                        futureDangerousBullets++;
                    }
                }
            }
            count++;
        }

        superDangerousBullets += getSuperBullets(this, this.getPlayer2Missiles(), timeTillFreeMissile);
        superDangerousBullets += getSuperBullets(this, this.getPlayer1AlienMissiles(), timeTillFreeMissile);
        shipX = Math.abs(this.getPlayer1SpawnSide() - this.getPlayer1X());
        timeTillRespawn = this.getPlayer1RespawnTime();
        numAliens = this.getPlayer1Aliens().size();
        //System.out.println(curr.getMapString());
        //futureAlienValue = getAlienValue(curr);
        futureKills = curr.getPlayer1Kills() - this.getPlayer1Kills();

        height = getPlayer1AlienWaveHeight();
        width = getPlayer1AlienWaveWidth();
        extremeWeights = this.getExtremeWeights();
        futureExtremeWeights = curr.getExtremeWeights();

        this.next = (SimpleState) curr;
    }
    /*Working
     public double calculateStateValue() {
     if (player1Lost()) {
     return -1000;
     }
     double value = getPlayer1Kills();
     value += -35 * missileMisses;
     value += 35 * lives;
     //value += 0.2 * currentAlienValue;
     value += futureAlienValue;
     value += -1 * dangerousBullets;
     value += 5 * futureBulletMisses;
     value += -1 * futureDangerousBullets;
     value += 40 * missileFactory;
     value += -10 * alienFactory;
     //value += (-5 * squaredDistanceFromSpawnSide) / 144.0;
     //value += (-10 * squaredDistanceFromNonSpawnSide) / 144.0;
     value += (-30 * squaredDistanceFromMissileFactory) / 144.0;
     //value += (-5 * unusedBullets);
     value += (-30 * superDangerousBullets);
     return value;
     }
     */

    public double calculateStateValue() throws InterruptedException {
        final SimpleState thisState = this;
        double value = 0;
        value += getBasicValue();

        killValue = 2.5 * Math.exp(-Math.pow(200 - this.getRound(), 2) / 4000.0) / Math.sqrt(40 * Math.PI) + 1;
        this.aliens = 3;
        if (this.getRound() + this.getPlayer1Player1SpawnTime() > 40) {
            aliens++;
        }
        if (this.player2AlienFactory()) {
            aliens++;
        }
        aliens *= 3;

        LinkedList<Callable<Double>> tasks = new LinkedList<>();

        tasks.add((Callable<Double>) () -> {
            next.futureDangerousBullets = this.futureDangerousBullets;
            return getValueOfState(next);
        });
        tasks.add((Callable<Double>) () -> {
            SimpleState s = shootPolicy2(thisState);
            return getValueOfState(s);
        });

        tasks.add((Callable<Double>) () -> {

            SimpleState s = shootPolicy(thisState);
            return getValueOfState(s);

        });

        tasks.add((Callable<Double>) () -> {

            SimpleState s = strafePolicy2(thisState);
            return getValueOfState(s);

        });

        tasks.add((Callable<Double>) () -> {

            SimpleState s = strafePolicy3(thisState);
            return getValueOfState(s);

        });

        tasks.add((Callable<Double>) () -> {
            SimpleState s = strafePolicy(thisState);
            return getValueOfState(s);

        });

        tasks.add((Callable<Double>) () -> {
            SimpleState s = strafePolicy7(thisState);
            return getValueOfState(s);
        });

        tasks.add((Callable<Double>) () -> {
            SimpleState s = strafePolicy4(thisState);
            return getValueOfState(s);
        });
        
         tasks.add((Callable<Double>) () -> {
         SimpleState s = strafePolicy5(thisState);
         return getValueOfState(s);
         });
         
        
         tasks.add((Callable<Double>) () -> {
         SimpleState s = strafePolicy8(thisState);
         return getValueOfState(s);
         });
         
        LinkedList<Future<Double>> futures = new LinkedList<>();
        for (Callable<Double> t : tasks) {

            futures.add(executor.submit(t));
        }
        double stateVal = Double.NEGATIVE_INFINITY;
        int count = 0;
        int best = 0;
        for (Future<Double> t : futures) {
            try {
                double v = t.get();
                if (v > stateVal) {
                    stateVal = v;
                    best = count;

                }
            } catch (InterruptedException ex) {
                throw ex;
            } catch (ExecutionException ex) {
                Logger.getLogger(SimpleState.class.getName()).log(Level.SEVERE, null, ex);
            }
            count++;
        }
        //SimpleState.taskChooseTimes[best]++;
        stateVal = Math.max(stateVal, getValueOfState(next));
        value += stateVal;

        return value;
    }

    public double getValueOfState(SimpleState s) {
        if (s.player1Lost() && s.getRound() < 200) {
            return -1000.0;
        }
        double value = 0;

        if (s.getRound() >= 200) {
            return value;
        }
        if (this.player1MissileFactory() && !s.player1MissileFactory()) {
            value -= 15;
        }
        if (this.getPlayer1Lives() > s.getPlayer1Lives() || !player1ShipAlive()) {
            value -= 15;
        }
        if (this.player1AlienFactory() && !s.player1AlienFactory()) {
            value -= 10;
        }
        value -= 2 * Math.min(s.futureDangerousBullets,4);
        value -= 0.2 * s.player1.getShields().size();

        if (this.player1MissileFactory() && !this.player1AlienFactory()) {
            s.distanceFromMissileFactory = Math.abs(s.getPlayer1X() - this.getPlayer1MissileFactory());
        }
        if (!this.player1MissileFactory() && !this.player1AlienFactory()) {
            s.distanceFromMissileFactory = Math.abs(s.getPlayer1SpawnSide() - s.getPlayer1X());
        } else if (this.player1AlienFactory() && this.player1MissileFactory()) {
            s.distanceFromMissileFactory = Math.abs(s.getPlayer1X() - (this.getPlayer1MissileFactory() + this.getPlayer1AlienFactory()) / 2.0);
        } else if (this.player1AlienFactory()) {
            s.distanceFromMissileFactory = Math.abs(s.getPlayer1X() - this.getPlayer1AlienFactory());
        }

        value += -0.8 * Math.min(s.futureDangerousBullets, 4) * (s.distanceFromMissileFactory);
        //if (player1MissileFactory()) {
        //    value -= (Math.abs(16 - s.getPlayer1SpawnSide() - this.getPlayer1MissileFactory())) * (killValue - 1);
        //}
        if (this.player2AlienFactory() || s.getPlayer1AlienWaveHeight() >= 7) {
            //value -= 1 * s.distanceFromMissileFactory;
            value += 0.3 * s.getPlayer1Kills() * killValue;
            value -= Math.pow(Math.max(s.getPlayer1AlienWaveHeight() - 3, 0) / 2.0, 1.5);
            value -= 2 * s.closeAlienWeights();
            //if (s.getPlayer1AlienWaveHeight() > 4) {
            //value += getAlienValue(s);
            if (this.player1MissileFactory()) {
                if (this.getPlayer1MissileFactory() > 8) {
                    value -= 4 * (16 - s.getPlayer1X());
                } else {
                    value -= 4 * (s.getPlayer1X());
                }
            } else {
                value -= 56;
            }
            double alienVal = getAlienValue(s);
            value += alienVal;
            //value -= distanceFromMissileFactory;
            //double height = ((getPlayer1AlienWaveHeight() - 1) / 2.0) + 1;
            double sweetSpot = 3;
            value += (-1 * Math.max(2*sweetSpot - s.getPlayer1Aliens().size(), 0) * alienVal) / sweetSpot;
//            if (s.getRound() < 200) {
//                double val = 0;
//                val += s.getPlayer1Aliens().size() / killValue;
//                val += 0.9 * Math.max((aliens - (s.getPlayer1Player1SpawnTime() / 3.0)), 0) / killValue;
//                double height = ((getPlayer1AlienWaveHeight() - 1) / 2.0) + 1;
//                value += ((2.0 * sweetSpot - (s.getPlayer1Aliens().size() + height)) / sweetSpot) * val;
//            }

//            } else {
//
//                value += s.getPlayer1Aliens().size() / killValue;
//                value += Math.max((aliens - (s.getPlayer1Player1SpawnTime() / 3.0)), 0) / killValue;
//
//            }
            //if (this.player1MissileFactory() && (Math.abs((16 - s.getPlayer1SpawnSide()) - this.getPlayer1MissileFactory()) <= 5)) {
            //    value += 20;
            //}
        } else {
            value += s.getPlayer1Kills() * killValue;
            value -= s.closeAlienWeights();

            value += s.getPlayer1Aliens().size() / killValue;
            value += 0.9 * Math.max((aliens - (s.getPlayer1Player1SpawnTime() / 3.0)), 0) / killValue;

        }
        return value;
    }

    public SimpleState doNothingPolicy(SimpleState curr, int times) {
        for (int i = 0; i < times; i++) {
            if (curr.getRound() >= 200) {
                break;
            }

            curr = curr.getNextState(Move.NONE, Move.NONE);
            updateStateBullets(curr);
        }
        curr.futureDangerousBullets = this.futureDangerousBullets + curr.futureDangerousBullets;
        return curr;
    }

    public SimpleState shootPolicy(SimpleState curr) {
        for (int i = 0; i < 12; i++) {
            curr = curr.getNextState(Move.FIRE, Move.NONE);
        }
        return curr;
    }

    public SimpleState shootPolicy2(SimpleState curr) {
        if (curr.getRound() >= 200) {
            return curr;
        }
        //System.out.println(curr.getMapString());
        int lastFire = 0;
        int bottomY = 11;
        for (Point2D m : curr.getPlayer1Missiles()) {
            bottomY = Math.max(bottomY, (int) m.getY());
        }
        if (bottomY < 19) {
            lastFire = 2;
        }
        if (bottomY == 19) {
            lastFire = 1;
        }

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }

            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            if (lastFire == 0) {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            } else if (lastFire == 1) {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            } else if (lastFire >= 2) {
                if (curr.getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (curr.getNextState(Move.NONE, Move.NONE).getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (curr.getPlayer1AlienDelta() == -1 && curr.getPlayer1X() > 1) {
                    curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
                } else if (curr.getPlayer1AlienDelta() == 1 && curr.getPlayer1X() < 15) {
                    curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
                } else {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                }
            }
            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
            lastFire++;
        }
        return curr;
    }

    public SimpleState strafePolicy(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }
        int direction = 1;
        int destination = 0;
        if (this.player1MissileFactory()) {
            if (this.getPlayer1MissileFactory() > 8) {
                direction = 1;
            } else {
                direction = -1;
            }
        }
        if (direction == 1) {
            destination = 13;
        } else {
            destination = 3;
        }

        int lastFire = 0;

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }
            int bottomY = 11;
            int bottomX = 0;
            for (Point2D m : curr.getPlayer1Missiles()) {
                if ((int) m.getY() > bottomY) {
                    bottomY = (int) m.getY();
                    bottomX = (int) m.getX();
                }
            }
            //if (bottomY == 18 && Math.abs(bottomX-curr.getPlayer1X()) == 1) {
            //    lastFire = 3;
            //}
            if (bottomY == 20) {
                lastFire = 0;
            } else if (bottomY == 18) {
                lastFire = 2;
            } else if (bottomY == 19) {
                lastFire = 1;
            } else {
                lastFire = 3;
            }

            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            if (lastFire >= 2) {
                if (curr.getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (curr.getNextState(Move.NONE, Move.NONE).getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (direction == -1 && curr.getPlayer1X() > 1) {
                    curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
                } else if (direction == 1 && curr.getPlayer1X() < 15) {
                    curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
                } else {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                }
            } else if (curr.getPlayer1X() < destination && direction == 1) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else if ((curr.getPlayer1X() > destination && direction == -1)) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else if (lastFire == 0) {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            } else if (lastFire == 1) {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            }
            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
            lastFire++;
        }
        return curr;
    }

    public SimpleState strafePolicy2(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }
        int direction = 1;
        int destination = 0;
        if (this.player1MissileFactory()) {
            if (this.getPlayer1MissileFactory() > 8) {
                direction = 1;
            } else {
                direction = -1;
            }
        }
        if (curr.getPlayer1SpawnSide() == 0) {
            if (direction == 1) {
                destination = 13;
            } else {
                destination = 3;
            }
        } else if (curr.getPlayer1SpawnSide() == 16) {
            if (direction == 1) {
                destination = 13;
            } else {
                destination = 3;
            }
        }

        int lastFire = 0;

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }
            int bottomY = 11;
            int bottomX = 0;
            for (Point2D m : curr.getPlayer1Missiles()) {
                if ((int) m.getY() > bottomY) {
                    bottomY = (int) m.getY();
                    bottomX = (int) m.getX();
                }
            }
            //if (bottomY == 18 && Math.abs(bottomX-curr.getPlayer1X()) == 1) {
            //    lastFire = 3;
            //}
            if (bottomY == 20) {
                lastFire = 0;
            } else if (bottomY == 18) {
                lastFire = 2;
            } else if (bottomY == 19) {
                lastFire = 1;
            } else {
                lastFire = 3;
            }

            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            if (curr.getPlayer1X() < destination && direction == 1) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else if ((curr.getPlayer1X() > destination && direction == -1)) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else if (lastFire == 0) {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            } else if (lastFire >= 1) {
                if (curr.getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (curr.getNextState(Move.NONE, Move.NONE).getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (direction == -1 && curr.getPlayer1X() > 1) {
                    curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
                } else if (direction == 1 && curr.getPlayer1X() < 15) {
                    curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
                } else {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                }
            }
            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
            lastFire++;
        }
        return curr;
    }

    public SimpleState strafePolicy7(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }
        int count = 0;
        int spawnTime = 0;
        for (GameState g : allStates) {
            if (g.player1.aliensSpawned) {
                spawnTime = count;
            }
            count++;
        }
        spawnTime++;
        int direction = 1;
        int destination = 0;
        if (this.player1MissileFactory()) {
            if (this.getPlayer1MissileFactory() > 8) {
                direction = 1;
            } else {
                direction = -1;
            }
        }
        int disp = 1;
        if (curr.getPlayer1SpawnSide() == 0) {
            if (direction == 1) {
                destination = 14;
                disp = 1;
            } else {
                disp = 1;
                destination = 2;
            }
        } else if (curr.getPlayer1SpawnSide() == 16) {
            if (direction == 1) {
                disp = -1;
                destination = 14;
            } else {
                disp = -1;
                destination = 2;
            }
        }

        int lastFire = 0;

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }
            int bottomY = 11;
            int bottomX = 0;
            for (Point2D m : curr.getPlayer1Missiles()) {
                if ((int) m.getY() > bottomY) {
                    bottomY = (int) m.getY();
                    bottomX = (int) m.getX();
                }
            }
            //if (bottomY == 18 && Math.abs(bottomX-curr.getPlayer1X()) == 1) {
            //    lastFire = 3;
            //}
            if (bottomY == 20) {
                lastFire = 0;
            } else if (bottomY == 18) {
                lastFire = 2;
            } else if (bottomY == 19) {
                lastFire = 1;
            } else {
                lastFire = 3;
            }
            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            if (spawnTime - i == 9 && curr.getPlayer1X() == destination) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (spawnTime - i == 8 && curr.getPlayer1X() == (destination + disp)) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (spawnTime - i == 10 && curr.getPlayer1X() == (destination - disp)) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (curr.getPlayer1X() < destination && direction == 1) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else if ((curr.getPlayer1X() > destination && direction == -1)) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else if (lastFire == 0) {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            } else if (lastFire >= 1 && spawnTime - i <= 8) {
                if (curr.getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (curr.getNextState(Move.NONE, Move.NONE).getPlayer1Missiles().size() < curr.getPlayer1MissileLimit()) {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                } else if (direction == -1 && curr.getPlayer1X() > 1) {
                    curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
                } else if (direction == 1 && curr.getPlayer1X() < 15) {
                    curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
                } else {
                    curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
                    lastFire = -1;
                }
            } else {
                curr = curr.getNextState(Move.NONE, Move.NONE, bullets);
            }
            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
            lastFire++;
        }
        return curr;
    }

    public SimpleState strafePolicy3(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }
        int direction = 1;
        int destination = 0;
        if (this.player1MissileFactory()) {
            if (this.getPlayer1MissileFactory() > 8) {
                direction = 1;
            } else {
                direction = -1;
            }
        }
        if (curr.getPlayer1SpawnSide() == 0) {
            if (direction == 1) {
                destination = 15;
            } else {
                destination = 1;
            }
        } else if (curr.getPlayer1SpawnSide() == 16) {
            if (direction == 1) {
                destination = 15;
            } else {
                destination = 1;
            }
        }

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }

            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            if (curr.getPlayer1X() < destination && direction == 1) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else if ((curr.getPlayer1X() > destination && direction == -1)) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);

            }

            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
        }
        return curr;
    }

    public SimpleState strafePolicy4(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }
            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            int bottomY = 11;
            int bottomX = 0;
            for (Point2D m : curr.getPlayer1Missiles()) {
                if ((int) m.getY() > bottomY) {
                    bottomY = (int) m.getY();
                    bottomX = (int) m.getX();
                }
            }
            int direction = 0;
            if (allStates.size() > i + 3) {
                direction = allStates.get(i + 3).getPlayer1AlienDelta();
            }

            if (bottomY <= 18) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (direction == 1) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else if (direction == -1) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            }

            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
        }
        return curr;
    }

    public SimpleState strafePolicy8(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }

        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }
            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);
            int bottomY = 11;
            int bottomX = 0;
            for (Point2D m : curr.getPlayer1Missiles()) {
                if ((int) m.getY() > bottomY) {
                    bottomY = (int) m.getY();
                    bottomX = (int) m.getX();
                }
            }
            int direction = 0;
            if (allStates.size() > i + 4) {
                direction = allStates.get(i + 4).getPlayer1AlienDelta();
            }

            if (bottomY <= 18) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (direction == 1) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else if (direction == -1) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            }

            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
        }
        return curr;
    }

    public SimpleState strafePolicy5(SimpleState curr) {
        //System.out.println(curr.getMapString());
        if (curr.getRound() >= 200) {
            return curr;
        }
        int direction = 1;
        int destination = 0;
        if (this.player1MissileFactory()) {
            if (this.getPlayer1MissileFactory() > 8) {
                direction = 1;
            } else {
                direction = -1;
            }
        }

        if (direction == 1) {
            destination = 1;
        } else {
            destination = 15;
        }
        for (int i = 0; i < 12; i++) {
            if (curr.getRound() >= 200) {
                return curr;
            }

            LinkedList<Point2D> bullets = updateStateBullets(curr);
            List<Point2D> bulls = getAllBullets(curr);

            if (curr.getSpaceInvaderObjectAt(new Point2D(curr.getPlayer1X() + curr.getPlayer1AlienDelta() * -1, 20)) != SpaceInvaderObject.EMPTY) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (curr.getSpaceInvaderObjectAt(new Point2D(curr.getPlayer1X() + curr.getPlayer1AlienDelta() * -1, 19)) != SpaceInvaderObject.EMPTY) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            } else if (curr.getSpaceInvaderObjectAt(new Point2D(curr.getPlayer1X() + 2 * curr.getPlayer1AlienDelta() * -1, 19)) != SpaceInvaderObject.EMPTY) {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);

            } else if (direction == -1 && curr.getPlayer1X() > 1) {
                curr = curr.getNextState(Move.LEFT, Move.NONE, bullets);
            } else if (direction == 1 && curr.getPlayer1X() < 15) {
                curr = curr.getNextState(Move.RIGHT, Move.NONE, bullets);
            } else {
                curr = curr.getNextState(Move.FIRE, Move.NONE, bullets);
            }

            if (bullets.isEmpty()) {
                if (this.aliensGuaranteedToFire(bulls, curr)) {
                    curr.player1.setShotEnergy(curr.player1.getShotEnergy() - 6);
                }
            }
            //System.out.println(curr.getMapString());
        }
        return curr;
    }

    public LinkedList<Point2D> updateStateBullets(SimpleState g) {
        List<Point2D> bullets = getAllBullets(g);
        Iterator<Point2D> bulletIt = bullets.iterator();
        LinkedList<Point2D> bulls = new LinkedList<>();
        out:
        while (bulletIt.hasNext()) {
            Point2D b = bulletIt.next();
            Iterator<Point2D> miss = g.getPlayer1Missiles().iterator();
            while (miss.hasNext()) {
                Point2D missile = miss.next();
                if (b.getX() == missile.getX() && b.getY() < missile.getY()) {
                    bulls.add(b);
                    continue out;
                }
            }
            if (this.player1MissileFactory() && Math.abs(b.getX() - getPlayer1MissileFactory()) <= 1) {
                g.futureDangerousBullets++;
            }
            if (this.player1AlienFactory() && Math.abs(b.getX() - getPlayer1AlienFactory()) <= 1) {
                g.futureDangerousBullets++;
            }
        }
        return bulls;
    }

    public double getBasicValue() {
        if (!Double.isNaN(basicValue)) {
            return basicValue;
        }
        this.initialiseStateVariables();
        if (player1Lost()) {
            return -1000.0 + this.getRound();
        }
        if (getRound() == 200) {
            return this.getPlayer1Kills();
        }
        double value = getPlayer1Kills();
        //value -= 0.2 * player1.getShields().size();
        value += -20 * missileMisses;
        value += 60 * lives;
        //value += currentAlienValue;
        //value += 0.5 * futureAlienValue;
        value += -1 * dangerousBullets;
        value += 5 * futureBulletMisses;
        value += 70 * missileFactory;
//        if (next.getPlayer2Aliens().size() > 12 || next.getPlayer2AlienWaveHeight() >= 8 || (next.getPlayer2Aliens().size() > 9 && Math.abs(this.getPlayer2SpawnSide() - this.getPlayer2X()) <= 5)) {
//            if (this.getRound() < 130) {
//                value += 40 * alienFactory;
//            }
//        }

        int height = (this.getPlayer2AlienWaveHeight() -1)/2 + 1;
        //build alien factory mode
        if (this.round > 40) {
            value += ((200 - round) / 6.4) * alienFactory + (27 + 2 * player2.getAliens().size() + 4*height) * alienFactory;
        }
        if (this.player1AlienFactory() && this.player1MissileFactory()) {
            if (Math.abs(this.getPlayer1MissileFactory() - this.getPlayer1AlienFactory()) != 3) {
                value -= 200;
            }
        }

        //value += (-5 * squaredDistanceFromSpawnSide) / 144.0;
        //value += (-10 * squaredDistanceFromNonSpawnSide) / 144.0;
        //value += (-30 * squaredDistanceFromMissileFactory) / 144.0;
        //value += (-5 * unusedBullets);
        //value -= 1.5*distanceFromMissileFactory;
        //if (this.getRound() > 45 && this.player2AlienFactory()) {
        //    value += -2 * next.distanceFromOptPosition;
        //} else {
        value += -1 * (Math.min(dangerousBullets, 3) + 0.2) * (distanceFromMissileFactory);
        
                    if (this.player1MissileFactory()) {
                if (this.getPlayer1MissileFactory() > 8) {
                    value -= 1.5 * (16 - this.getPlayer1X());
                } else {
                    value -= 1.5 * (this.getPlayer1X());
                }
            } else {
                value -= 21;
            }

        //value -= 0.2*Math.abs(16 - this.getPlayer1SpawnSide() - this.getPlayer1X());
        //value -= next.distanceToAliens();
        // }
        ///if (next.getPlayer1Aliens().size() > 17 || next.getPlayer1AlienWaveHeight() >= 8 || (next.getPlayer1Aliens().size() > 9 && this.player2AlienFactory())) {
        // if (this.getPlayer1Lives() > 0) {
        //    value += -2 * distanceFromOptPosition;
        //}
        // else {
        //     value -= 24;
        // }
        //}
        if (this.player1MissileFactory() && (this.getPlayer1MissileFactory() >= 5 && this.getPlayer1MissileFactory() <= 11)) {
            value -= 50;
        }
        if (this.player1MissileFactory() && (this.getPlayer1MissileFactory() < 3 || this.getPlayer1MissileFactory() > 13)) {
            value -= 50;
        }

        value += (-25 * superDangerousBullets);
        //value -= 0.2*expectedTimeToKill;
        //value += 2*futureKills;
        basicValue = value;
        return value;
    }

    public boolean canIntercept(Point2D bullet, GameState curr) {
        if (bullet.getX() == 0 || bullet.getX() == 16) {
            return false;
        }
        int yDist = 19 - (int) bullet.getY();
        int xDist = Math.abs(curr.getPlayer1X() - (int) bullet.getX());
        if (xDist <= yDist) {
            return true;
        } else {
            return false;
        }
    }

    public boolean canIntercept(Point2D bullet, GameState curr, int timeTillFreeMissile) {
        if (bullet.getX() == 0 || bullet.getX() == 16) {
            return false;
        }
        int yDist = 19 - (int) bullet.getY();
        int xDist = Math.abs(curr.getPlayer1X() - (int) bullet.getX());
        if (xDist <= yDist && timeTillFreeMissile <= yDist) {
            return true;
        } else {
            return false;
        }
    }

    public boolean canDodge(Point2D bullet, GameState curr) {
        int yDist = 20 - (int) bullet.getY();
        int leftPos = (int) bullet.getX() - 2;
        int rightPos = (int) bullet.getX() + 2;
        int xDist = 16;
        if (leftPos > 0) {
            xDist = Math.min(xDist, Math.abs(curr.getPlayer1X() - leftPos));
        }
        if (rightPos < 16) {
            xDist = Math.min(xDist, Math.abs(curr.getPlayer1X() - rightPos));
        }
        if (xDist <= yDist) {
            return true;
        } else {
            return false;
        }
    }

    public double getBulletValue(GameState g, List<Point2D> incoming) {
        double value = 0;
        out:
        for (Point2D bullet : incoming) {
            if (bullet.getY() <= 11) {
                continue;
            }
            for (Point2D missile : g.getPlayer1Missiles()) {
                if (missile.getX() == bullet.getX() && missile.getY() > bullet.getY()) {
                    continue out;
                }
            }
            if (Math.abs(bullet.getX() - g.getPlayer1X()) <= 1) {
                value += 1;
            } else if (g.player1MissileFactory()) {
                if (Math.abs(bullet.getX() - g.getPlayer1MissileFactory()) <= 1) {
                    value += 1;
                }
            }
            if (g.player1AlienFactory()) {
                if (Math.abs(bullet.getX() - g.getPlayer1AlienFactory()) <= 1) {
                    value += 1;
                }
            }
        }
        return value;
    }

    public double bestStateDistance() {
        double dist = this.getPlayer1Player1SpawnTime() + getPlayer1Aliens().size();
        return Math.min(expectedTimeToKillAliens(this), dist);
    }

    public double expectedTimeToKillAliens(GameState g) {
        int freeTime = 0;
        if (g == this) {
            freeTime = timeTillFreeMissile;
        } else {
            freeTime = 0;
        }
        double totalDistance = 0;
        for (Point2D alien : g.getPlayer1Aliens()) {
            totalDistance += Math.abs(20 - alien.getY());
        }
        return (totalDistance + freeTime) / g.getPlayer1MissileLimit();
    }

    public double getSuperBullets(GameState g, List<Point2D> incoming, int timeTillFreeMissile) {
        double value = 0;
        out:
        for (Point2D bullet : incoming) {
            if (bullet.getY() <= 11) {
                continue;
            }
            for (Point2D missile : g.getPlayer1Missiles()) {
                if (missile.getX() == bullet.getX() && missile.getY() > bullet.getY()) {
                    continue out;
                }
            }
            if (Math.abs(bullet.getX() - g.getPlayer1X()) <= 1) {
                if (!canDodge(bullet, g) && !canIntercept(bullet, g, timeTillFreeMissile)) {
                    value += 1;
                }
            }
            if (g.player1MissileFactory()) {
                if (Math.abs(bullet.getX() - g.getPlayer1MissileFactory()) <= 1) {
                    if (!canIntercept(bullet, g, timeTillFreeMissile)) {
                        value += 1;
                    }
                }
            }
            if (g.player1AlienFactory()) {
                if (Math.abs(bullet.getX() - g.getPlayer1AlienFactory()) <= 1) {
                    if (!canIntercept(bullet, g, timeTillFreeMissile)) {
                        value += 1;
                    }
                }
            }
        }
        return value;
    }

    //public double[] getFeatures() {
    //    return LFA.getAllFeatures(this);
    //}
    public double getAlienValue(GameState state) {
        int aliens = 3;
        if (this.getRound() + state.getPlayer1Player1SpawnTime() > 40) {
            aliens++;
        }
        if (this.player2AlienFactory()) {
            aliens++;
        }
        aliens *= 3;

        double v = -5.2 * Math.max((aliens - (state.getPlayer1Player1SpawnTime() / 3.0)), 0);
        v = v - state.getPlayer1OldAlienWeights();

        return v;

    }

    public double getAlienValueKills(GameState state) {

        double v = -7 * Math.max((5 - (state.getPlayer1Player1SpawnTime() / 3.0)), 0);
        v = v - 2.5 * state.getPlayer1AlienWaveHeight() - state.getPlayer1OldAlienWeights() / 2.0;
        v += state.getPlayer1AlienWaveWidth() * 10;
        v += state.getPlayer1Kills();

        return v;

    }

    public double getFutureWeight() {
        initialiseStateVariables();
        return 0;
    }

    public double getCurrentWeight() {
        initialiseStateVariables();
        return this.currentAlienValue;
    }

    public List<Point2D> getAllBullets(GameState curr) {
        List<Point2D> bullets = new LinkedList<>();
        if (curr.getPlayer1AlienPrimaryBullet() != null) {
            bullets.add(curr.getPlayer1AlienPrimaryBullet());
        }
        bullets.addAll(curr.getPlayer1AlienSecondaryBullets());
        return bullets;
    }

    public boolean aliensGuaranteedToFire(List<Point2D> bullets, GameState next) {
        if (bullets.size() <= 1) {
            return false;
        }
        for (Point2D bullet : bullets) {
            if (!next.getPlayer1Aliens().contains(new Point2D(bullet.getX(), bullet.getY() - 1))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public double getReward(GameState previous) {

        int kills = previous.getPlayer1Kills();
        int lives = previous.getPlayer1Lives();
        boolean shipWasAlive = previous.player1ShipAlive();
        boolean hadMissileFactory = previous.player1MissileFactory();
        double reward = this.getPlayer1Kills() - kills;
        reward += 5 * (this.getPlayer1Lives() - lives);
        if (this.player1MissileFactory() && !hadMissileFactory) {
            reward += 5;
        }
        if (!this.player1ShipAlive() && shipWasAlive) {
            reward -= 10;
        }
        if (!this.player1MissileFactory() && hadMissileFactory) {
            reward -= 10;
        }
        if (this.isGameOver() && this.getRound() < 200) {
            reward -= 50;
        }
        if (this.player1.alienFreshSpawn) {
            reward += 10;
        }
        return reward;
    }

    public double getReward(GameState previous, GameState thisState) {
        int kills = previous.getPlayer1Kills();
        int lives = previous.getPlayer1Lives();
        boolean shipWasAlive = previous.player1ShipAlive();
        boolean hadMissileFactory = previous.player1MissileFactory();
        double reward = thisState.getPlayer1Kills() - kills;
        reward += 5 * (thisState.getPlayer1Lives() - lives);
        if (!hadMissileFactory && thisState.player1MissileFactory()) {
            reward += 5;
        }
        if (!thisState.player1ShipAlive() && shipWasAlive) {
            reward -= 10;
        }
        if (!thisState.player1MissileFactory() && hadMissileFactory) {
            reward -= 10;
        }
        if (thisState.isGameOver() && thisState.getRound() < 200) {
            reward -= 50;
        }
        return reward;
    }

}
