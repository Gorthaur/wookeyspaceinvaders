package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class OnSpawnSideFeature extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 2;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        if (Math.abs(g.getPlayer1SpawnSide() - g.getPlayer1X()) > 8) {
            return new double[]{1, 0};
        } else {
            return new double[]{0, 1};
        }
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        if (Math.abs(g.getPlayer1SpawnSide() - g.getPlayer1X()) > 8) {
            return new int[]{0};
        } else {
            return new int[]{1};
        }
    }

}
