package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class SpawnColumnFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 7;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        double values[] = new double[7];
        int x;
        if (g.getPlayer1SpawnSide() == 0) {
            x = g.player1.getLeftmostAlienX();
        } else {
            x = g.player1.getRightmostAlienX();
        }
        int yOffset = g.getYOffset();

        int numInCols = g.getColSize(x, yOffset);
        int count = 0;
        for (int numInCol = 0; numInCol < 7; numInCol++) {
            if (numInCols == numInCol) {
                values[count] = 1;
            } else {
                values[count] = 0;
            }
            count++;
        }
        return values;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int x;
        if (g.getPlayer1SpawnSide() == 0) {
            x = g.player1.getLeftmostAlienX();
        } else {
            x = g.player1.getRightmostAlienX();
        }
        int yOffset = g.getYOffset();

        int numInCols = g.getColSize(x, yOffset);
        if (numInCols > 6) {
            numInCols = 6;
        }
        int count = 0;
        return new int[]{numInCols};
    }

}
