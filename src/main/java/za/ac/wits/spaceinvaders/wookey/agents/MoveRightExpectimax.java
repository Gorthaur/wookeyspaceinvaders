package za.ac.wits.spaceinvaders.wookey.agents;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import za.ac.wits.spaceinvaders.wookey.Agent;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.PlayerMove;
import za.ac.wits.spaceinvaders.wookey.ThreadedAgent;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveLeftOpening;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpening;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpeningFive;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpeningThree;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpeningThreeRandom;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpeningTwo;
import za.ac.wits.spaceinvaders.wookey.openings.MoveLeftOpening;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class MoveRightExpectimax extends Expectimax implements TestableAgent {

    public MoveRightExpectimax() {
        this.setOpening(new FireMoveRightOpeningFive());
    }

    @Override
    public String getDescription() {
        return "Right Expectimax";
    }


}
