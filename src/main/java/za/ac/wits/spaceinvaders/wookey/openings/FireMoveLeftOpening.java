
package za.ac.wits.spaceinvaders.wookey.openings;

import za.ac.wits.spaceinvaders.wookey.Opening;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class FireMoveLeftOpening extends Opening {

    @Override
    public Move[] getOpeningMoves() {
        return new Move[]{Move.FIRE, Move.LEFT,Move.LEFT,Move.LEFT,Move.LEFT,Move.LEFT,Move.LEFT,Move.FIRE, Move.FIRE, Move.LEFT, Move.FIRE, Move.FIRE, Move.LEFT, Move.FIRE, Move.FIRE, Move.RIGHT,Move.BUILD_MISSILE_FACTORY};
    }
    
}
