package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class BulletFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 12;
    }

    @Override
    public double[] getFeatureValues(GameState g) {

        if (g instanceof SimpleState) {

            SimpleState s = (SimpleState) g;
            s.initialiseStateVariables();
            double killValue = 5 * Math.exp(-Math.pow(200 - s.getRound(), 2) / 40.0) / Math.sqrt(40 * Math.PI) + 1;

            int aliens = s.getRound() < 40 ? 3 : 4;
            if (s.player2AlienFactory()) {
                aliens++;
            }
            double total = s.next.getPlayer1Aliens().size();
            total += Math.max((aliens - (s.next.getPlayer1Player1SpawnTime() / 3.0)), 0);
            return new double[]{s.dangerousBullets / 2.0, s.futureBulletMisses / 3.0, s.futureDangerousBullets / 2.0, (s.futureKills * killValue) / 2.0, s.lives / 3.0, s.missileMisses / 2.0, s.superDangerousBullets / 3.0, (200 - s.getRound()) / 200.0, total/70.0, s.distanceFromOptPosition / 11.0, s.distanceFromMissileFactory / 12.0, s.distanceToAliens() / 25.0};
        }
        return new double[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
        double f[] = getFeatureValues(g);
        int count = 0;
        for (int i = 0; i < f.length; i++) {
            if (f[i] != 0.0) {
                count++;
            }
        }
        int ret[] = new int[count];
        int c = 0;
        for (int i = 0; i < f.length; i++) {
            if (f[i] != 0.0) {
                ret[c] = i;
                c++;
            }
        }
        return ret;
    }

    @Override
    public double[] getValues(GameState g) {
        double f[] = getFeatureValues(g);
        int count = 0;
        for (int i = 0; i < f.length; i++) {
            if (f[i] != 0.0) {
                count++;
            }
        }
        double ret[] = new double[count];
        int c = 0;
        for (int i = 0; i < f.length; i++) {
            if (f[i] != 0.0) {
                ret[c] = f[i];
                c++;
            }
        }
        return ret;
    }

}
