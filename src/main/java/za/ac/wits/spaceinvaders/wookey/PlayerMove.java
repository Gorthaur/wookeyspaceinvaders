package za.ac.wits.spaceinvaders.wookey;

import java.util.LinkedList;
import javafx.geometry.Point2D;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class PlayerMove {

    Move p1Move;
    Move p2Move;
    Point2D bullets[];
    GameState result;

    public void setBullets(Point2D... bullets) {
        this.bullets = bullets;
    }

    public Move getP1Move() {
        return p1Move;
    }

    public Move getP2Move() {
        return p2Move;
    }

    public GameState getResult() {
        return result;
    }
}
