package za.ac.wits.spaceinvaders.wookey.agents;

import java.util.ArrayList;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public class AlienMove implements Comparable<AlienMove> {

    public GameState result;
    public Point2D bullet;
    public ArrayList<MoveValue> children;
    public double probability;
    public double value;

    public AlienMove(GameState result, Point2D bullet, double value) {
        this.result = result;
        this.bullet = bullet;
        this.value = value;
    }
    
        public AlienMove(GameState result, Point2D bullet, double value, double probability) {
        this.result = result;
        this.bullet = bullet;
        this.value = value;
        this.probability = probability;
    }

    @Override
    public String toString() {
        if (children != null && !children.isEmpty()) {
            if (bullet == null) {
                return  "no bullet " + children.get(0).toString();
            }
            return bullet + " " + children.get(0).toString();
        } else {
            if (bullet == null) {
                return "no bullet leaf node";
            }
            return bullet + " leaf node";
        }
    }

    @Override
    public int compareTo(AlienMove o) {
        if (value < o.value) {
            return -1;
        } else if (value > o.value) {
            return 1;
        } else {
            return 0;
        }
    }
}
