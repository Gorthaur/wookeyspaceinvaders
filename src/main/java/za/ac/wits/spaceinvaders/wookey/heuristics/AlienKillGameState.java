package za.ac.wits.spaceinvaders.wookey.heuristics;

import java.io.IOException;
import java.io.InputStream;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class AlienKillGameState extends SimpleState {

    public AlienKillGameState(InputStream in) throws IOException {
        super(in);
    }

    protected AlienKillGameState(GameState previous, Move player1Move, Move player2Move) {
        this(previous, player1Move, player2Move, new Point2D[0]);
    }

    protected AlienKillGameState(GameState previous, Move player1Move, Move player2Move, Point2D[] bullets) {
        super(previous, player1Move, player2Move, bullets);
    }

    @Override
    public SimpleState getNextState(Move move, Move move1) {
        SimpleState g = new AlienKillGameState(this, move, move1);
        return g;
    }

    @Override
    public GameState getNextState(Move move, Move move1, Point2D... pds) {
        return new AlienKillGameState(this, move, move1, pds);
    }
    
    @Override
        public double calculateStateValue() {
            
        if (player1Lost()) {
            return -1000;
        }
        double value = getPlayer1Kills();
        value += -20 * missileMisses;
        value += 35 * lives;
        //value += 0.2 * currentAlienValue;
        value += -2 * dangerousBullets;
        value += 5 * futureBulletMisses;
        value += -3 * futureDangerousBullets;
        value += 40 * missileFactory;
        value += 500 * alienFactory;
        //value += (-5 * squaredDistanceFromSpawnSide) / 144.0;
        //value += (-30 * squaredDistanceFromMissileFactory) / 144.0;
        //value += (-5 * unusedBullets);
        value += (-10 * superDangerousBullets);
        return value;
                    
    }
        
    @Override
    public double getAlienValue(GameState state) {
        double v = state.getPlayer1Aliens().size()*0.9;
        return v + state.getPlayer1Kills() - 5*(state.getPlayer1AlienWaveHeight()-1);
    }
    

    
}
