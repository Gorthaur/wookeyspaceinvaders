package za.ac.wits.spaceinvaders.wookey;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
import javafx.geometry.Point2D;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class Player {

    protected int kills;
    protected int lives;
    /**
     * Direction the aliens attacking this player are going.
     */
    protected int delta;
    /**
     * Shot energy of aliens attacking this player.
     */
    protected int shotEnergy;
    /**
     * Wave size of aliens attacking this player
     */
    protected int waveSize;
    protected String name;
    protected boolean hasMissileFactory;
    protected boolean hasAlienFactory;
    protected int missileFactoryX;
    protected int alienFactoryX;
    protected int shipX;
    protected int respawnTime;
    public int centreDistance = 23;
    public int maxCentreDistance = 0;
    protected int leftStrength = 0;
    protected int rightStrength = 0;
    protected int bottomRowAliens = 0;
    protected int side;
    /**
     * Missiles fired by this player
     */
    LinkedList<Point2D> missiles = new LinkedList<>();
    /**
     * Aliens attacking this player
     */
    private LinkedList<Point2D> aliens = new LinkedList<>();

    int leftmostAlienX = 16;
    int rightmostAlienX = 0;

    /**
     * Missiles from aliens fired towards this player.
     */
    LinkedList<Point2D> alienMissiles = new LinkedList<>();

    LinkedList<Point2D> shields = new LinkedList<>();
    public boolean alienFreshSpawn;
    public boolean aliensSpawned;

    /**
     * -1 for top, 1 for bottom
     *
     * @param side
     */
    public Player(int side) {
        this.side = side;
    }

    public boolean isAlive() {
        if (shipX == 0) {
            return false;
        } else {
            return true;
        }
    }

    public void reloadLimits() {
        this.leftmostAlienX = 16;
        this.rightmostAlienX = 0;
        this.centreDistance = 23;
        this.maxCentreDistance = 0;
        this.leftStrength = 0;
        this.rightStrength = 0;
        bottomRowAliens = 0;
        for (Point2D alien : aliens) {
            leftmostAlienX = (int) Math.min(leftmostAlienX, alien.getX());
            rightmostAlienX = (int) Math.max(rightmostAlienX, alien.getX());
            centreDistance = Math.min(centreDistance, (int) Math.abs(11 - alien.getY()));
            if ((int) Math.abs(11 - alien.getY()) == maxCentreDistance) {
                bottomRowAliens++;
            } else if ((int) Math.abs(11 - alien.getY()) > maxCentreDistance) {
                bottomRowAliens = 1;
            }
            maxCentreDistance = Math.max(maxCentreDistance, (int) Math.abs(11 - alien.getY()));
        }
    }

    public void addAlien(Point2D alien) {
        aliens.add(alien);
        if (alien.getX() == leftmostAlienX) {
            this.leftStrength++;
        } else if (alien.getX() < leftmostAlienX) {
            this.leftStrength = 1;
        }

        if (alien.getX() == rightmostAlienX) {
            this.rightStrength++;
        } else if (alien.getX() > rightmostAlienX) {
            this.rightStrength = 1;
        }
        leftmostAlienX = (int) Math.min(leftmostAlienX, alien.getX());
        rightmostAlienX = (int) Math.max(rightmostAlienX, alien.getX());
        centreDistance = Math.min(centreDistance, (int) Math.abs(11 - alien.getY()));
        if ((int) Math.abs(11 - alien.getY()) == maxCentreDistance) {
            bottomRowAliens++;
        } else if ((int) Math.abs(11 - alien.getY()) > maxCentreDistance) {
            bottomRowAliens = 1;
        }
        maxCentreDistance = Math.max(maxCentreDistance, (int) Math.abs(11 - alien.getY()));
    }

    public void addShield(Point2D shield) {
        shields.add(shield);
    }

    public LinkedList<Point2D> getAliens() {
        return aliens;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public int getShotEnergy() {
        return shotEnergy;
    }

    public void setShotEnergy(int shotEnergy) {
        this.shotEnergy = shotEnergy;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.kills;
        hash = 89 * hash + this.lives;
        hash = 89 * hash + this.delta;
        hash = 89 * hash + this.shotEnergy;
        hash = 89 * hash + this.waveSize;
        hash = 89 * hash + Objects.hashCode(this.name);
        hash = 89 * hash + (this.hasMissileFactory ? 1 : 0);
        hash = 89 * hash + (this.hasAlienFactory ? 1 : 0);
        hash = 89 * hash + this.missileFactoryX;
        hash = 89 * hash + this.alienFactoryX;
        hash = 89 * hash + this.shipX;
        hash = 89 * hash + this.respawnTime;
        hash = 89 * hash + this.side;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (this.kills != other.kills) {
            return false;
        }
        if (this.lives != other.lives) {
            return false;
        }
        if (this.delta != other.delta) {
            return false;
        }
        if (this.shotEnergy != other.shotEnergy) {
            return false;
        }
        if (this.waveSize != other.waveSize) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.hasMissileFactory != other.hasMissileFactory) {
            return false;
        }
        if (this.hasAlienFactory != other.hasAlienFactory) {
            return false;
        }
        if (this.missileFactoryX != other.missileFactoryX) {
            return false;
        }
        if (this.alienFactoryX != other.alienFactoryX) {
            return false;
        }
        if (this.shipX != other.shipX) {
            return false;
        }
        if (this.respawnTime != other.respawnTime) {
            return false;
        }
        if (this.side != other.side) {
            return false;
        }
        if (!Objects.equals(this.missiles, other.missiles)) {
            return false;
        }
        if (!Objects.equals(this.aliens, other.aliens)) {
            return false;
        }
        if (!Objects.equals(this.alienMissiles, other.alienMissiles)) {
            return false;
        }
        if (!Objects.equals(this.shields, other.shields)) {
            return false;
        }
        return true;
    }

    public int getWaveSize() {
        return waveSize;
    }

    public void setWaveSize(int waveSize) {
        this.waveSize = waveSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasMissileFactory() {
        return hasMissileFactory;
    }

    public void setHasMissileFactory(boolean hasMissileFactory) {
        this.hasMissileFactory = hasMissileFactory;
    }

    public boolean isHasAlienFactory() {
        return hasAlienFactory;
    }

    public void setHasAlienFactory(boolean hasAlienFactory) {
        this.hasAlienFactory = hasAlienFactory;
    }

    public int getMissileFactoryX() {
        return missileFactoryX;
    }

    public void setMissileFactoryX(int missileFactoryX) {
        this.missileFactoryX = missileFactoryX;
    }

    public int getAlienFactoryX() {
        return alienFactoryX;
    }

    public void setAlienFactoryX(int alienFactoryX) {
        this.alienFactoryX = alienFactoryX;
    }

    public int getShipX() {
        return shipX;
    }

    public void setShipX(int shipX) {
        this.shipX = shipX;
    }

    public int getRespawnTime() {
        return respawnTime;
    }

    public void setRespawnTime(int respawnTime) {
        this.respawnTime = respawnTime;
    }

    public int getNumMissiles() {
        return missiles.size();
    }

    public int getMissileLimit() {
        if (this.hasMissileFactory) {
            return 2;
        } else {
            return 1;
        }
    }

    public LinkedList<Point2D> getMissiles() {
        return missiles;
    }

    public void setMissiles(LinkedList<Point2D> missiles) {
        this.missiles = missiles;
    }

    public int getLeftmostAlienX() {
        return leftmostAlienX;
    }

    public void setLeftmostAlienX(int leftmostAlienX) {
        this.leftmostAlienX = leftmostAlienX;
    }

    public int getRightmostAlienX() {
        return rightmostAlienX;
    }

    public void setRightmostAlienX(int rightmostAlienX) {
        this.rightmostAlienX = rightmostAlienX;
    }

    public LinkedList<Point2D> getAlienMissiles() {
        return alienMissiles;
    }

    public void setAlienMissiles(LinkedList<Point2D> alienMissiles) {
        this.alienMissiles = alienMissiles;
    }

    public LinkedList<Point2D> getShields() {
        return shields;
    }

    public void setShields(LinkedList<Point2D> shields) {
        this.shields = shields;
    }

    public void spawnAliens(LinkedList<Point2D> aliens, int waveSize) {

        for (Point2D alien : aliens) {
            addAlien(alien);
        }
        if (centreDistance >= 3) {
            this.aliensSpawned = true;
            int y;
            if (side == 1) {
                y = 12;
            } else {
                y = 10;
            }
            int firstAlienX = 0;
            if (aliens.size() == 0) {
                alienFreshSpawn = true;
                if (delta == -1) {
                    firstAlienX = 16;
                } else {
                    firstAlienX = 0;
                }
            } else if (delta == -1) {
                firstAlienX = rightmostAlienX;
            } else {
                firstAlienX = leftmostAlienX;
            }
            while (!inBoundsX(firstAlienX + 3 * delta * (waveSize - 1))) {
                firstAlienX -= 3 * delta;
            }

            for (int i = 0; i < waveSize; i++) {
                addAlien(new Point2D(firstAlienX, y));
                firstAlienX += 3 * delta;
            }
        }
    }

    public void moveAliens() {
        LinkedList<Point2D> oldAliens = aliens;
        if (oldAliens.size() == 0) {
            return;
        }
        aliens = new LinkedList<>();
        int deltaX = 0;
        int deltaY = 0;

        if (delta == -1 && leftmostAlienX == 0) {
            deltaY = side;
            delta = 1;
        } else if (delta == 1 && rightmostAlienX == 16) {
            deltaY = side;
            delta = -1;
        } else {
            deltaX = delta;
        }
        this.leftmostAlienX = 16;
        this.rightmostAlienX = 0;
        this.centreDistance = 23;
        this.maxCentreDistance = 0;
        this.leftStrength = 0;
        this.rightStrength = 0;
        bottomRowAliens = 0;

        for (Point2D alien : oldAliens) {
            addAlien(new Point2D(alien.getX() + deltaX, alien.getY() + deltaY));
        }

    }

    public boolean inBoundsX(int x) {
        if (x >= 0 && x <= 16) {
            return true;
        } else {
            return false;
        }
    }

    void spawnMissiles(LinkedList<Point2D> missiles) {
        for (Point2D missile : missiles) {
            this.missiles.add(missile);
        }
    }

    void spawnBullets(LinkedList<Point2D> alienMissiles) {
        for (Point2D alienMissile : alienMissiles) {
            this.alienMissiles.add(alienMissile);
        }
    }

    void spawnShip(int shipX) {
        this.shipX = shipX;
    }

    void spawnAlienFactory(int alienFactoryX) {
        this.alienFactoryX = alienFactoryX;
        if (alienFactoryX > 0) {

            hasAlienFactory = true;
        } else {
            hasAlienFactory = false;
        }
    }

    void spawnMissileFactory(int missileFactoryX) {
        this.missileFactoryX = missileFactoryX;
        if (missileFactoryX > 0) {
            hasMissileFactory = true;
        } else {
            hasMissileFactory = false;
        }
    }

    void spawnShields(LinkedList<Point2D> shields) {
        for (Point2D shield : shields) {
            this.shields.add(shield);
        }
    }

    void moveMissiles() {
        LinkedList<Point2D> oldMissiles = this.missiles;
        missiles = new LinkedList<>();
        for (Point2D missile : oldMissiles) {
            missiles.add(new Point2D(missile.getX(), missile.getY() - side));
        }
    }

    public void moveBullets() {
        LinkedList<Point2D> oldBullets = this.alienMissiles;
        this.alienMissiles = new LinkedList<>();
        for (Point2D missile : oldBullets) {
            alienMissiles.add(new Point2D(missile.getX(), missile.getY() + side));
        }
    }

    public void executeMove(Move move) {
        int y;
        if (side == -1) {
            y = 1;
        } else {
            y = 21;
        }
        if (isAlive()) {
            if (move == Move.LEFT) {
                if (this.shipX >= 2) {
                    this.shipX -= 1;
                }
            } else if (move == Move.RIGHT) {
                if (this.shipX <= 14) {
                    this.shipX += 1;
                }
            } else if (move == Move.FIRE) {
                if (this.missiles.size() < this.getMissileLimit()) {
                    missiles.add(new Point2D(shipX, y - side));
                    missiles.sort(PointComparator.getInstance());
                }
            } else {
                if (this.getLives() > 0) {
                    if (move == Move.BUILD_ALIEN_FACTORY) {
                        if (!this.hasAlienFactory) {
                            if (this.hasMissileFactory) {
                                if (Math.abs(this.missileFactoryX - shipX) >= 3) {
                                    alienFactoryX = shipX;
                                    hasAlienFactory = true;
                                    lives--;
                                }
                            } else {
                                alienFactoryX = shipX;
                                hasAlienFactory = true;
                                lives--;
                            }
                        }
                    }
                    if (move == Move.BUILD_MISSILE_FACTORY) {
                        if (!this.hasMissileFactory) {
                            if (this.hasAlienFactory) {
                                if (Math.abs(this.alienFactoryX - shipX) >= 3) {
                                    missileFactoryX = shipX;
                                    hasMissileFactory = true;
                                    lives--;
                                }
                            } else {
                                missileFactoryX = shipX;
                                hasMissileFactory = true;
                                lives--;
                            }
                        }
                    }

                    if (move == Move.BUILD_SHIELDS) {
                        lives--;
                        for (int i = -1; i < 2; i++) {
                            for (int j = -3; j < 0; j++) {
                                Point2D shield = new Point2D(shipX + i, y + side * j);
                                if (!shields.contains(shield)) {
                                    shields.add(shield);
                                }
                            }
                        }
                        Collections.sort(shields, PointComparator.getInstance());
                    }
                }
            }
        }
    }

}
