package za.ac.wits.spaceinvaders.wookey;

import java.util.LinkedList;
import java.util.List;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public abstract class Opening {

    public Move getMove(GameState g) {
        Move openingMoves[] = getOpeningMoves();
        if (g.round < openingMoves.length) {
            return flipMove(openingMoves[g.round], g.getPlayer1SpawnSide());
        } else {
            return null;
        }
    }

    public List<Move> getMoves(GameState g) {
        Move openingMoves[] = getOpeningMoves();
        if (g.round < openingMoves.length) {
            LinkedList<Move> moves = new LinkedList<>();
            moves.add(flipMove(openingMoves[g.round], g.getPlayer1SpawnSide()));
            return moves;
        } else {
            return null;
        }
    }

    public abstract Move[] getOpeningMoves();

    public Move flipMove(Move m, int spawnSide) {
        if (spawnSide == 16) {
            return m;
        } else {
            return GameState.flipMove(m);
        }
    }
}
