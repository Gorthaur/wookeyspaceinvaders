package za.ac.wits.spaceinvaders.wookey;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

public abstract class Agent implements TestableAgent {

    private Opening opening;

    public void setOpening(Opening opening) {
        this.opening = opening;
    }

    @Override
    public GameState getGameState(InputStream in) throws IOException {
        return new GameState(in);
    }

    @Override
    public Move getMove(InputStream in) throws IOException {
        GameState gameState = getGameState(in);
        return getMove(gameState);
    }

    public List<Move> getMoves(InputStream in) throws IOException {
        GameState gameState = getGameState(in);
        return getMoves(gameState);
    }

    public Move getMove(GameState state) {
        if (opening != null) {
            Move m = opening.getMove(state);
            if (m != null) {
                return m;
            }
        }
        return getCalculatedMove(state);
    }

    public List<Move> getMoves(GameState state) {
        if (opening != null) {
            List<Move> m = opening.getMoves(state);
            if (m != null) {
                return m;
            }
        }
        return getCalculatedMoves(state);
    }

    public abstract Move getCalculatedMove(GameState state);

    public abstract List<Move> getCalculatedMoves(GameState state);

    public long getMoveTime() {
        return 2100;
    }

    @Override
    public String toString() {
        return getDescription();
    }

}
