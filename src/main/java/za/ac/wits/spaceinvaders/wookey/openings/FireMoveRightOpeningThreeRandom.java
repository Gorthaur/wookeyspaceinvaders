
package za.ac.wits.spaceinvaders.wookey.openings;

import za.ac.wits.spaceinvaders.wookey.Opening;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class FireMoveRightOpeningThreeRandom extends Opening {

    @Override
    public Move[] getOpeningMoves() {
        return new Move[]{Move.FIRE, Move.RIGHT,Move.RIGHT,Move.RIGHT,Move.RIGHT,Move.BUILD_MISSILE_FACTORY, Move.RIGHT, Move.FIRE, Move.FIRE, Move.FIRE, getRandomMove(), getRandomMove(), getRandomMove(), getRandomMove(), getRandomMove(), getRandomMove(), getRandomMove(), getRandomMove()};
    }
    
    public static Move getRandomMove() {
        Move[] bestMoves = new Move[]{Move.FIRE, Move.LEFT, Move.RIGHT, Move.NONE, Move.BUILD_MISSILE_FACTORY};
        return bestMoves[(int)(Math.random()*bestMoves.length)];
    }
    
}
