package za.ac.wits.spaceinvaders.wookey.heuristics;

import java.io.IOException;
import java.io.InputStream;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class AlienKillNoFactoryGameState extends SimpleState {

    public AlienKillNoFactoryGameState(InputStream in) throws IOException {
        super(in);
    }

    protected AlienKillNoFactoryGameState(GameState previous, Move player1Move, Move player2Move) {
        this(previous, player1Move, player2Move, new Point2D[0]);
    }

    protected AlienKillNoFactoryGameState(GameState previous, Move player1Move, Move player2Move, Point2D[] bullets) {
        super(previous, player1Move, player2Move, bullets);
    }

    @Override
    public SimpleState getNextState(Move move, Move move1) {
        SimpleState g = new AlienKillNoFactoryGameState(this, move, move1);
        return g;
    }

    @Override
    public GameState getNextState(Move move, Move move1, Point2D... pds) {
        return new AlienKillNoFactoryGameState(this, move, move1, pds);
    }

    public double calculateStateValue() {

        if (player1Lost()) {
            return -1000;
        }
        double value = getPlayer1Kills();
        value += -20 * missileMisses;
        value += 35 * lives;
        //value += 0.2 * currentAlienValue;
        value += -2 * dangerousBullets;
        value += 5 * futureBulletMisses;
        value += -3 * futureDangerousBullets;
        value += 40 * missileFactory;
        value += -10 * alienFactory;
        //value += distanceFromSpawnSide;
        //value += (-5 * squaredDistanceFromSpawnSide) / 144.0;
        value += (-30 * squaredDistanceFromMissileFactory) / 144.0;
        //value += (-5 * unusedBullets);
        value += (-30 * superDangerousBullets);
        return value;

    }

    @Override
    public double getAlienValue(GameState state) {
        if (expectedTimeToKillAliens(this) <= state.getPlayer1Player1SpawnTime()) {
            return 2*state.getPlayer1Kills() + state.getPlayer1Aliens().size();
        } else {
            double v = Math.max((5 - (state.getPlayer1Player1SpawnTime() / 3.0)), 0) + 1.9*state.getPlayer1Kills();
            return v - 5 * (state.getPlayer1AlienWaveHeight() - 1) + state.getPlayer1AlienWeights() / 14.0;
        }
        //double v = state.getPlayer1Aliens().size() + 0.1*Math.max((4 - (state.getPlayer1Player1SpawnTime() / 3.0)), 0);
        //return v + 1.1*state.getPlayer1Kills() - 5*(state.getPlayer1AlienWaveHeight()-1);

    }

}
