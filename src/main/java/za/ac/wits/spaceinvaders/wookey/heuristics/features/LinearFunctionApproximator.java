package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public abstract class LinearFunctionApproximator {
    
    public double getValue(GameState state) {
        int activeFeatures[] = getActiveFeatures(state);
        double activeValues[] = getActiveFeatureValues(state);
        //getTestVal(state, activeFeatures, activeValues);
        return getValue(activeFeatures, activeValues);
    }
    
    public void getTestVal(GameState state, int activeFeatures[], double activeValues[]) {
        int actives[] = new int[activeFeatures.length];
        double d[] = getAllFeatures(state);
        
        int count = 0;
        for (int i = 0; i < d.length; i++) {
            if (count < activeValues.length && activeValues[count] == 0.0) {
                count++;
            }
            if (d[i] != 0.0) {
                actives[count] = i;
                if (activeFeatures[count] != i) {
                    throw new RuntimeException("woah");
                }
                if (d[i] != activeValues[count]) {
                    throw new RuntimeException("woah2");
                }
                
                count++;
                
            }
        }
    }

    public double getValue(int activeFeatures[], double activeValues[]) {
        HashMap<List<Integer>, Double> values = getValueMap();
        double value = 0;
        //get independent
        for (int i = 0; i < activeFeatures.length; i++) {
            List<Integer> p = Arrays.asList(activeFeatures[i]);
            if (values.containsKey(p)) {
                value += values.get(p)*activeValues[i];
            }
        }
        //get pairs
        for (int i = 0; i < activeFeatures.length; i++) {
            for (int j = i + 1; j < activeFeatures.length; j++) {
                List<Integer> p = Arrays.asList(activeFeatures[i], activeFeatures[j]);
                if (values.containsKey(p)) {
                    value += values.get(p)*activeValues[i]*activeValues[j];
                }
            }
        }

        
        return value;
    }

    public double[] getAllFeatures(GameState state) {
        FeatureSet sets[] = getFeatureSets();
        int numFeatures = 0;
        for (FeatureSet set : sets) {
            numFeatures += set.getNumFeatures();
        }

        double features[] = new double[numFeatures];
        int count = 0;
        for (FeatureSet set : sets) {
            double f[] = set.getFeatureValues(state);
            for (double d : f) {
                features[count] = d;
                count++;
            }
        }
        return features;
    }

    public int[] getActiveFeatures(GameState state) {
        LinkedList<Integer> activeFeatureIndices = new LinkedList<>();
        int offset = 0;
        for (FeatureSet set : getFeatureSets()) {
            int[] actives = set.getActiveFeatures(state);
            for (int i = 0; i < actives.length; i++) {
                activeFeatureIndices.add(actives[i] + offset);
            }

            offset += set.getNumFeatures();
        }

        int[] actives = new int[activeFeatureIndices.size()];
        int count = 0;
        for (Integer i : activeFeatureIndices) {
            actives[count] = i;
            count++;
        }
        return actives;
    }
    
    public double[] getActiveFeatureValues(GameState g) {
        LinkedList<Double> activeFeatureValues = new LinkedList<>();
        for (FeatureSet set : getFeatureSets()) {
            double activeValues[] = set.getValues(g);
            for (int i = 0; i < activeValues.length; i++) {
                activeFeatureValues.add(activeValues[i]);
            }
        }

        double values[] = new double[activeFeatureValues.size()];
        int count = 0;
        for (Double d: activeFeatureValues) {
            values[count] = d;
            count++;
        }
        //Arrays.sort(actives);
        return values;
    }

    public abstract FeatureSet[] getFeatureSets();

    public abstract HashMap<List<Integer>, Double> getValueMap();
}
