package za.ac.wits.spaceinvaders.wookey.agents;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.Agent;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.PlayerMove;
import za.ac.wits.spaceinvaders.wookey.ThreadedAgent;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class Expectimax extends SimpleMiniMax implements TestableAgent {

    ///public Expectimax() {
    //}

    @Override
    public String getDescription() {
        return "Expectimax";
    }

    @Override
    public GameState getGameState(InputStream in) throws IOException {
        return new SimpleState(in);
    }

    @Override
    public void alphaBetaAlien(AlienMove parentParent, MoveValue parent, int depth, double alpha, double beta, ConcurrentHashMap<GameState, Double> values) throws InterruptedException {
        checkInterrupted();
        double value = 0;
        for (AlienMove aMove : parent.children) {
            alphaBeta(parentParent,aMove, depth - 1, alpha, beta, values);
            double v = aMove.probability * aMove.value;
            value += v;
        }
       Collections.sort(parent.children);
        parent.value = value;
    }

}
