package za.ac.wits.spaceinvaders.wookey;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.agents.AlienMove;
import za.ac.wits.spaceinvaders.wookey.agents.MoveValue;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.SpaceInvaderObject;
import za.ac.wits.witstestablespaceinvaders.TestableGameState;

public class GameState implements TestableGameState, Serializable {

    public ArrayList<MoveValue> moveValues = null;
    public static Move moves[] = new Move[]{Move.NONE, Move.FIRE, Move.BUILD_ALIEN_FACTORY, Move.BUILD_MISSILE_FACTORY, Move.BUILD_SHIELDS, Move.LEFT, Move.RIGHT};
    static Point2D cardinal[] = new Point2D[]{new Point2D(-1, -1), new Point2D(-1, 0), new Point2D(-1, 1), new Point2D(0, -1), new Point2D(0, 1), new Point2D(1, -1), new Point2D(1, 0), new Point2D(1, 1)};
    protected int round = 0;
    public final Player player1;
    public final Player player2;
    protected SpaceInvaderObject board[][];

    protected Point2D primaryBullet1 = null;
    protected Point2D primaryBullet2 = null;

    protected LinkedList<Point2D> secondaryBullet1 = new LinkedList<>();
    protected LinkedList<Point2D> secondaryBullet2 = new LinkedList<>();
    protected boolean spawnPlayer2 = true;

    protected GameState(GameState previous, Move player1Move, Move player2Move) {
        this(previous, player1Move, player2Move, new Point2D[0]);
    }

    protected GameState(GameState previous, Move player1Move, Move player2Move, List<Point2D> bullets) {
        this(previous, player1Move, player2Move, bullets.toArray(new Point2D[]{}));
    }

    protected GameState(GameState previous, Move player1Move, Move player2Move, Point2D[] bullets) {
        this.spawnPlayer2 = previous.spawnPlayer2;
        player1 = new Player(1);
        player2 = new Player(-1);
        round = previous.getRound() + 1;

        player1.delta = previous.player1.delta;
        player2.delta = previous.player2.delta;

        player1.spawnMissiles(previous.player1.getMissiles());
        player1.spawnBullets(previous.player1.getAlienMissiles());
        player1.spawnShip(previous.player1.getShipX());
        player1.spawnAlienFactory(previous.player1.getAlienFactoryX());
        player1.spawnMissileFactory(previous.player1.getMissileFactoryX());
        player1.spawnShields(previous.player1.getShields());
        player1.lives = previous.player1.lives;
        player1.kills = previous.player1.kills;
        player1.name = previous.player1.name;
        player1.shotEnergy = previous.player1.shotEnergy + 1;
        player1.respawnTime = previous.player1.respawnTime;

        player2.spawnMissiles(previous.player2.getMissiles());
        player2.spawnBullets(previous.player2.getAlienMissiles());
        player2.spawnShip(previous.player2.getShipX());
        player2.spawnAlienFactory(previous.player2.getAlienFactoryX());
        player2.spawnMissileFactory(previous.player2.getMissileFactoryX());
        player2.spawnShields(previous.player2.getShields());
        player2.lives = previous.player2.lives;
        player2.kills = previous.player2.kills;
        player2.name = previous.player2.name;
        player2.shotEnergy = previous.player2.shotEnergy + 1;
        player2.respawnTime = previous.player2.respawnTime;

        setWaveSize();
        player1.spawnAliens(previous.player1.getAliens(), player1.waveSize);
        if (this.spawnPlayer2) {
            player2.spawnAliens(previous.player2.getAliens(), player2.waveSize);
        }

        Collections.sort(player1.getAliens(), PointComparator.getInstance());
        Collections.sort(player2.getAliens(), PointComparator.getInstance());
        this.collissionCheckAfterSpawn(player1, player2);
        this.collissionCheckAfterSpawn(player2, player1);

        LinkedList<Point2D> player1OldMissiles = player1.getMissiles();
        LinkedList<Point2D> player2OldMissiles = player2.getMissiles();
        player1.moveMissiles();
        player2.moveMissiles();

        //remove missiles that pass through each other
        LinkedList<Point2D> overlapping = getCollissions(1, player1.getMissiles(), player2OldMissiles);
        player1.getMissiles().removeAll(overlapping);
        LinkedList<Point2D> overlapping2 = getCollissions(1, player2.getMissiles(), player1OldMissiles);
        player2.getMissiles().removeAll(overlapping2);

        LinkedList<Point2D> collissions = this.collissionCheckAfterMissiles(player1, player2);
        LinkedList<Point2D> collissions2 = this.collissionCheckAfterMissiles(player2, player1);
        collissions.addAll(collissions2);
        collissions.addAll(this.getCollissions(1, player1.getMissiles(), player2.getMissiles()));
        player1.getMissiles().removeAll(collissions);
        player2.getMissiles().removeAll(collissions);

        player1.moveBullets();
        player2.moveBullets();

        collissionCheckAfterBullets(player1, player2);
        collissionCheckAfterBullets(player2, player1);

        player1.moveAliens();
        player2.moveAliens();

        collissionCheckAfterAliens(player1, player2);
        collissionCheckAfterAliens(player2, player1);

        //ALIENS SHOOT HERE
        aliensShoot(bullets);
        Collections.sort(player1.getAlienMissiles(), PointComparator.getInstance());
        Collections.sort(player2.getAlienMissiles(), PointComparator.getInstance());
        collissionCheckAfterBullets(player1, player2);
        collissionCheckAfterBullets(player2, player1);

        //player orders here
        if (player1Move == Move.BUILD_ALIEN_FACTORY || player1Move == Move.BUILD_MISSILE_FACTORY) {
            if (this.buildingAreaFree(player1, player2)) {
                player1.executeMove(player1Move);
            }
        } else {
            player1.executeMove(player1Move);
        }
        if (player2Move == Move.BUILD_ALIEN_FACTORY || player2Move == Move.BUILD_MISSILE_FACTORY) {
            if (this.buildingAreaFree(player2, player1)) {
                player2.executeMove(flipMove(player2Move));
            }
        } else {
            player2.executeMove(flipMove(player2Move));
        }

        postMoveCollissionCheck(player1, player2, player1Move);
        postMoveCollissionCheck(player2, player1, player2Move);

        //remove everything covered by a shield
        //blow up buildings from alien bullets, enemy bullets and aliens
        //do a missile move collission check
        //spawnPlayer1Aliens(previous.getPlayer1Aliens(), previous.getPlayer1AlienDelta());
        //spawnPlayer2Aliens();
        //if (spawnAliens(1))
        if (player1.respawnTime != -1) {
            if (!player1.isAlive()) {
                player1.respawnTime -= 1;
            }
            if (player1.respawnTime <= 0 && !player1.isAlive()) {
                player1.shipX = 8;
                player1.lives--;
            }
        }
        if (player2.respawnTime != -1) {
            if (!player2.isAlive()) {
                player2.respawnTime -= 1;
            }
            if (player2.respawnTime <= 0 && !player2.isAlive()) {
                player2.shipX = 8;
                player2.lives--;
            }
        }

        postRespawnCollissionCheck(player1, player2);
        postRespawnCollissionCheck(player2, player1);
        player1.reloadLimits();
        player2.reloadLimits();

        setWaveSize();
        getAllAlienBulletsForNextState();
    }

    public GameState(InputStream in, boolean player2Spawn) throws IOException {
        this(in);
        spawnPlayer2 = false;
    }

    public GameState(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        player1 = new Player(1);
        player2 = new Player(-1);
        reader.readLine();
        player2.name = lineToName(reader.readLine());
        player1.waveSize = lineToWaveSize(reader.readLine());
        player1.delta = lineToDelta(reader.readLine());
        player1.shotEnergy = lineToEnergy(reader.readLine());
        player2.respawnTime = lineToRespawn(reader.readLine());
        round = lineToRound(reader.readLine());
        player2.kills = lineToKills(reader.readLine());
        player2.lives = lineToLives(reader.readLine());
        int missiles[] = lineToMissiles(reader.readLine());
        reader.readLine();//############

        readBoard(reader);

        reader.readLine();
        missiles = lineToMissiles(reader.readLine());
        player1.lives = lineToLives(reader.readLine());
        player1.kills = lineToKills(reader.readLine());
        reader.readLine(); //round
        player1.respawnTime = lineToRespawn(reader.readLine());
        player2.shotEnergy = lineToEnergy(reader.readLine());
        player2.delta = lineToDelta(reader.readLine());
        player2.waveSize = lineToWaveSize(reader.readLine());
        player1.name = lineToName(reader.readLine());
        getAllAlienBulletsForNextState();
    }

    public void aliensShoot(Point2D bullets[]) {
        for (Point2D bullet : bullets) {
            if (bullet.getY() < 11) {
                if (player2.shotEnergy >= 6) {
                    for (Point2D alien : player2.getAliens()) {
                        if (alien.getX() == bullet.getX() && (int) alien.getY() == (int) bullet.getY() + 1) {
                            player2.alienMissiles.add(bullet);
                            player2.shotEnergy -= 6;
                        }
                    }
                }
            }
            if (bullet.getY() > 11) {
                if (player1.shotEnergy >= 6) {
                    for (Point2D alien : player1.getAliens()) {
                        if (alien.getX() == bullet.getX() && (int) alien.getY() == (int) bullet.getY() - 1) {
                            player1.alienMissiles.add(bullet);
                            player1.shotEnergy -= 6;
                        }
                    }
                }
            }
        }
    }

    public void getAllAlienBulletsForNextState() {
        Player p1 = new Player(1);
        Player p2 = new Player(-1);
        p1.delta = player1.delta;
        p2.delta = player2.delta;
        int waveP1 = player1.waveSize;
        int waveP2 = player2.waveSize;
        if (round == 39) {
            waveP1++;
            waveP2++;
        }
        p1.spawnAliens(player1.getAliens(), waveP1);
        p2.spawnAliens(player2.getAliens(), waveP2);
        p1.getAliens().removeAll(player1.getMissiles());
        p1.getAliens().removeAll(player2.getMissiles());
        p2.getAliens().removeAll(player1.getMissiles());
        p2.getAliens().removeAll(player2.getMissiles());

        Point2D bulletDelta1;
        Point2D bulletDelta2;
        if (player1.delta == -1) {
            if (p1.leftmostAlienX == 0) {
                bulletDelta1 = new Point2D(0, 2);
            } else {
                bulletDelta1 = new Point2D(-1, 1);
            }
        } else {
            if (p1.rightmostAlienX == 16) {
                bulletDelta1 = new Point2D(0, 2);
            } else {
                bulletDelta1 = new Point2D(1, 1);
            }
        }

        if (player2.delta == -1) {
            if (p2.leftmostAlienX == 0) {
                bulletDelta2 = new Point2D(0, -2);
            } else {
                bulletDelta2 = new Point2D(-1, -1);
            }
        } else {
            if (p2.rightmostAlienX == 16) {
                bulletDelta2 = new Point2D(0, -2);
            } else {
                bulletDelta2 = new Point2D(1, -1);
            }
        }

        int ship1X;
        if (player1.isAlive()) {
            ship1X = player1.shipX;
        } else {
            ship1X = 8;
        }

        int ship2X;
        if (player2.isAlive()) {
            ship2X = player2.shipX;
        } else {
            ship2X = 8;
        }
        int firstRow = 11;
        int secondRow = 11;

        if (player1.shotEnergy >= 5) {
            for (Point2D alien : p1.getAliens()) {
                if (alien.getY() > firstRow) {
                    if (firstRow > secondRow) {
                        secondRow = firstRow;
                    }
                    firstRow = (int) alien.getY();
                } else if (alien.getY() > secondRow && alien.getY() != firstRow) {
                    secondRow = (int) alien.getY();
                }
            }
            if (firstRow > 11) {
                for (Point2D alien : p1.getAliens()) {
                    if ((int) alien.getY() == firstRow) {
                        if (primaryBullet1 == null || Math.abs(primaryBullet1.getX() - ship1X) > Math.abs((int) alien.getX() + bulletDelta1.getX() - ship1X)) {
                            primaryBullet1 = new Point2D((int) alien.getX() + (int) bulletDelta1.getX(), (int) alien.getY() + (int) bulletDelta1.getY());
                        }
                    }
                }
            }
            for (Point2D alien : p1.getAliens()) {
                if ((int) alien.getY() >= secondRow) {
                    if (this.getSpaceInvaderObjectAt(new Point2D(alien.getX(), alien.getY() + 2)) != SpaceInvaderObject.ALIEN) {
                        secondaryBullet1.add(new Point2D((int) alien.getX() + (int) bulletDelta1.getX(), (int) alien.getY() + (int) bulletDelta1.getY()));
                    }
                }
            }
            secondaryBullet1.remove(primaryBullet1);

        }

        firstRow = 11;
        secondRow = 11;
        if (player2.shotEnergy >= 5) {
            for (Point2D alien : p2.getAliens()) {
                if (alien.getY() < firstRow) {
                    if (firstRow < secondRow) {
                        secondRow = firstRow;
                    }
                    firstRow = (int) alien.getY();
                } else if (alien.getY() < secondRow && alien.getY() != firstRow) {
                    secondRow = (int) alien.getY();
                }
            }
            if (firstRow < 11) {
                for (Point2D alien : p2.getAliens()) {
                    if ((int) alien.getY() == firstRow) {
                        if (primaryBullet2 == null || Math.abs(primaryBullet2.getX() - ship2X) > Math.abs((int) alien.getX() + bulletDelta2.getX() - ship2X)) {
                            primaryBullet2 = new Point2D((int) alien.getX() + (int) bulletDelta2.getX(), (int) alien.getY() + (int) bulletDelta2.getY());
                        }
                    }
                }
            }
            for (Point2D alien : p2.getAliens()) {
                if ((int) alien.getY() <= secondRow) {
                    if (this.getSpaceInvaderObjectAt(new Point2D(alien.getX(), alien.getY() - 2)) != SpaceInvaderObject.ALIEN) {
                        secondaryBullet2.add(new Point2D((int) alien.getX() + (int) bulletDelta2.getX(), (int) alien.getY() + (int) bulletDelta2.getY()));
                    }
                }
            }
            secondaryBullet2.remove(primaryBullet2);

        }

    }

    public static Move flipMove(Move move) {
        if (move == Move.LEFT) {
            return Move.RIGHT;
        } else if (move == Move.RIGHT) {
            return Move.LEFT;
        } else {
            return move;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.round;
        hash = 97 * hash + Objects.hashCode(this.player1);
        hash = 97 * hash + Objects.hashCode(this.player2);
        hash = 97 * hash + Arrays.deepHashCode(this.board);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GameState other = (GameState) obj;
        if (this.round != other.round) {
            return false;
        }
        if (!Objects.equals(this.player1, other.player1)) {
            return false;
        }
        if (!Objects.equals(this.player2, other.player2)) {
            return false;
        }
        if (!Arrays.deepEquals(this.board, other.board)) {
            return false;
        }
        return true;
    }

    public int getPlayer1MissileLimit() {
        return player1.getMissileLimit();
    }

    public int getPlayer2MissileLimit() {
        return player2.getMissileLimit();
    }

    public void setWaveSize() {

        player1.waveSize = 3;
        player2.waveSize = 3;
        if (player1AlienFactory()) {
            player2.waveSize++;
        }
        if (player2AlienFactory()) {
            player1.waveSize++;
        }
        if (round >= 40) {
            player1.waveSize++;
            player2.waveSize++;
        }
    }

    private String cleanLine(String line) {
        line = line.trim();
        line = line.substring(1, line.length() - 1);
        line = line.trim();
        return line;
    }

    private String[] cleanAndSplit(String line) {
        line = cleanLine(line);
        String split[] = line.split("[\\s/]+");
        return split;
    }

    private String lineToName(String line) {
        return cleanLine(line);
    }

    private int lineToRound(String line) {
        return Integer.parseInt(cleanAndSplit(line)[1]);
    }

    private int lineToKills(String line) {
        return Integer.parseInt(cleanAndSplit(line)[1]);
    }

    private int lineToLives(String line) {
        return Integer.parseInt(cleanAndSplit(line)[1]);
    }

    private int lineToWaveSize(String line) {
        return Integer.parseInt(cleanAndSplit(line)[2]);
    }

    private int lineToDelta(String line) {
        return Integer.parseInt(cleanAndSplit(line)[2]);
    }

    private int lineToEnergy(String line) {
        return Integer.parseInt(cleanAndSplit(line)[1]);
    }

    private int lineToRespawn(String line) {
        return Integer.parseInt(cleanAndSplit(line)[1]);
    }

    private int[] lineToMissiles(String line) {
        int m[] = new int[2];
        String split[] = cleanAndSplit(line);
        m[0] = Integer.parseInt(split[1]);
        m[1] = Integer.parseInt(split[2]);
        return m;
    }

    protected void readBoard(BufferedReader reader) throws IOException {
        for (int i = 0; i < 23; i++) {
            String line = reader.readLine().trim();
            line = line.substring(1, line.length() - 1);
            char chars[] = line.toCharArray();
            int j = 0;
            while (j < chars.length) {
                if (chars[j] == 'M') {
                    if (i == 0) {
                        player2.missileFactoryX = j + 1;
                        player2.hasMissileFactory = true;
                    } else {
                        player1.missileFactoryX = j + 1;
                        player1.hasMissileFactory = true;
                    }
                    j += 2;
                } else if (chars[j] == 'X') {
                    if (i == 0) {
                        player2.alienFactoryX = j + 1;
                        player2.hasAlienFactory = true;
                    } else {
                        player1.alienFactoryX = j + 1;
                        player1.hasAlienFactory = true;
                    }
                    j += 2;
                } else if (chars[j] == 'V') {
                    player2.shipX = j + 1;
                    j += 2;
                } else if (chars[j] == 'A') {
                    player1.shipX = j + 1;
                    j += 2;
                } else if (chars[j] == '|') {
                    if (i <= 11) {
                        player2.alienMissiles.add(new Point2D(j, i));
                    } else {
                        player1.alienMissiles.add(new Point2D(j, i));
                    }

                } else if (chars[j] == '-') {
                    if (i <= 11) {
                        player2.shields.add(new Point2D(j, i));
                    } else {
                        player1.shields.add(new Point2D(j, i));
                    }
                } else if (chars[j] == 'i') {
                    player2.missiles.add(new Point2D(j, i));
                } else if (chars[j] == '!') {
                    player1.missiles.add(new Point2D(j, i));
                } else if (chars[j] == 'x') {
                    if (i <= 11) {
                        player2.addAlien(new Point2D(j, i));

                    } else {
                        player1.addAlien(new Point2D(j, i));
                    }
                }
                j++;
            }
        }
    }

    @Override
    public GameState getNextState(Move move, Move move1) {
        GameState g = new GameState(this, move, move1);
        return g;
    }

    @Override
    public GameState getNextState(Move move, Move move1, Point2D... pds) {
        return new GameState(this, move, move1, pds);
    }

    @Override
    public int getPlayer1X() {
        return player1.shipX;
    }

    @Override
    public int getPlayer2X() {
        return player2.shipX;
    }

    @Override
    public List<Point2D> getPlayer1Aliens() {
        return player1.getAliens();
    }

    @Override
    public List<Point2D> getPlayer2Aliens() {
        return player2.getAliens();
    }

    @Override
    public String getMapString() {
        if (board == null) {
            updateBoard();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("###################").append(System.lineSeparator());
        stringBuilder.append(String.format("# %-15s #", player2.getName())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Wave Size: %2d   #", player1.getWaveSize())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Delta X: %2d     #", player1.getDelta())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Energy: %2d/6    #", player1.getShotEnergy())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Respawn: %2d     #", player2.getRespawnTime())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Round: %3d      #", round)).append(System.lineSeparator());
        stringBuilder.append(String.format("# Kills: %3d      #", player2.getKills())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Lives: %1d        #", player2.getLives())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Missiles: %1d/%1d   #", player2.getMissiles().size(), player2.getMissileLimit())).append(System.lineSeparator());
        stringBuilder.append("###################").append(System.lineSeparator());
        for (int j = 0; j < board[0].length; j++) {//y
            stringBuilder.append("#");
            for (int i = 0; i < board.length; i++) {//x
                if (board[i][j] == null || board[i][j] == SpaceInvaderObject.EMPTY) {
                    stringBuilder.append(" ");
                } else if (board[i][j] == SpaceInvaderObject.ALIEN) {
                    stringBuilder.append("x");
                } else if (board[i][j] == SpaceInvaderObject.ALIEN_MISSILE) {
                    stringBuilder.append("|");
                } else if (board[i][j] == SpaceInvaderObject.SHIELD) {
                    stringBuilder.append("-");
                } else if (board[i][j] == SpaceInvaderObject.MISSILE_FACTORY) {
                    stringBuilder.append("M");
                } else if (board[i][j] == SpaceInvaderObject.ALIEN_FACTORY) {
                    stringBuilder.append("X");
                } else if (board[i][j] == SpaceInvaderObject.PLAYER1_MISSILE) {
                    stringBuilder.append("!");
                } else if (board[i][j] == SpaceInvaderObject.PLAYER2_MISSILE) {
                    stringBuilder.append("i");
                } else if (board[i][j] == SpaceInvaderObject.SHIP) {
                    if (j <= 11) {
                        stringBuilder.append("V");
                    } else {
                        stringBuilder.append("A");
                    }
                }
            }
            stringBuilder.append("#").append(System.lineSeparator());
        }
        stringBuilder.append("###################").append(System.lineSeparator());

        stringBuilder.append(String.format("# Missiles: %1d/%1d   #", player1.getMissiles().size(), player1.getMissileLimit())).append(System.lineSeparator());

        stringBuilder.append(String.format("# Lives: %1d        #", player1.getLives())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Kills: %3d      #", player1.getKills())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Round: %3d      #", round)).append(System.lineSeparator());
        stringBuilder.append(String.format("# Respawn: %2d     #", player1.getRespawnTime())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Energy: %2d/6    #", player2.getShotEnergy())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Delta X: %2d     #", player2.getDelta())).append(System.lineSeparator());
        stringBuilder.append(String.format("# Wave Size: %2d   #", player2.getWaveSize())).append(System.lineSeparator());
        stringBuilder.append(String.format("# %-15s #", player1.getName())).append(System.lineSeparator());
        stringBuilder.append("###################").append(System.lineSeparator());

        return stringBuilder.toString();
    }

    @Override
    public List<Point2D> getPlayer1Missiles() {
        return player1.getMissiles();
    }

    @Override
    public List<Point2D> getPlayer2Missiles() {
        return player2.getMissiles();
    }

    @Override
    public List<Point2D> getPlayer1AlienMissiles() {
        return player1.getAlienMissiles();
    }

    @Override
    public List<Point2D> getPlayer2AlienMissiles() {
        return player2.getAlienMissiles();
    }

    @Override
    public int getPlayer1Kills() {
        return player1.getKills();
    }

    @Override
    public int getPlayer2Kills() {
        return player2.getKills();
    }

    @Override
    public int getRound() {
        return round;
    }

    @Override
    public SpaceInvaderObject getSpaceInvaderObjectAt(Point2D pd) {
        if (board == null) {
            updateBoard();
        }
        int x = (int) pd.getX();
        int y = (int) pd.getY();
        if (x < 0 || x > 16 || y < 0 || y > 22) {
            return SpaceInvaderObject.EMPTY;
        }
        if (board[x][y] == null) {
            return SpaceInvaderObject.EMPTY;
        } else {
            return board[x][y];
        }
    }

    @Override
    public boolean isGameOver() {
        if (round == 200 || (player1.lives == 0 && player1.getShipX() == 0) || (player2.lives == 0 && player2.getShipX() == 0)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getWinner() {
        if (round == 200 || (player1.lives == 0 && player1.getShipX() == 0 && player2.lives == 0 && player2.getShipX() == 0)) {
            if (player1.getKills() > player2.getKills()) {
                return 1;
            } else if (player1.getKills() < player2.getKills()) {
                return 2;
            } else {
                return 1; //player 1 wins by default
            }
        }
        if (player1.isAlive()) {
            return 1;
        }
        //player 1 is dead currently, could still be the winner
        if (player2.isAlive()) {
            return 2;
        }
        if (player1.getLives() > player2.getLives()) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int getPlayer1Lives() {
        return player1.getLives();
    }

    @Override
    public int getPlayer2Lives() {
        return player2.getLives();
    }

    @Override
    public boolean player1AlienFactory() {
        return player1.hasAlienFactory;
    }

    @Override
    public boolean player2AlienFactory() {
        return player2.hasAlienFactory;
    }

    @Override
    public boolean player1MissileFactory() {
        return player1.hasMissileFactory;
    }

    @Override
    public boolean player2MissileFactory() {
        return player2.hasMissileFactory;
    }

    @Override
    public int getPlayer1MissileFactory() {
        return player1.getMissileFactoryX();
    }

    @Override
    public int getPlayer2MissileFactory() {
        return player2.getMissileFactoryX();
    }

    @Override
    public int getPlayer1AlienFactory() {
        return player1.getAlienFactoryX();
    }

    @Override
    public int getPlayer2AlienFactory() {
        return player2.getAlienFactoryX();
    }

    @Override
    public boolean player1Respawning() {
        return player1.respawnTime > -1;
    }

    @Override
    public boolean player2Respawning() {
        return player2.respawnTime > -1;
    }

    @Override
    public int getPlayer1RespawnTime() {
        return player1.respawnTime;
    }

    @Override
    public int getPlayer2RespawnTime() {
        return player2.respawnTime;
    }

    @Override
    public int getPlayer1WaveSize() {
        return player1.waveSize;
    }

    @Override
    public int getPlayer2WaveSize() {
        return player2.waveSize;
    }

    @Override
    public int getPlayer1AlienDelta() {
        return player1.delta;
    }

    @Override
    public int getPlayer2AlienDelta() {
        return player2.delta;
    }

    @Override
    public int getPlayer1AlienShotEnergy() {
        return player1.shotEnergy;
    }

    @Override
    public int getPlayer2AlienShotEnergy() {
        return player2.shotEnergy;
    }

    @Override
    public double getStateValue() throws InterruptedException {
        return player1.getKills();
    }

    @Override
    public String getStateDescription() {
        return "state";
    }

    @Override
    public boolean player1ShipAlive() {
        return player1.isAlive();
    }

    @Override
    public boolean player2ShipAlive() {
        return player2.isAlive();
    }

    @Override
    public String getPlayer1Name() {
        return player1.getName();
    }

    @Override
    public String getPlayer2Name() {
        return player2.getName();
    }

    public void updateBoard() {
        board = new SpaceInvaderObject[17][23];
        for (Point2D p : player1.getAliens()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.ALIEN;
        }
        for (Point2D p : player2.getAliens()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.ALIEN;
        }
        for (Point2D p : player1.getMissiles()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.PLAYER1_MISSILE;
        }
        for (Point2D p : player2.getMissiles()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.PLAYER2_MISSILE;
        }
        for (Point2D p : player1.getAlienMissiles()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.ALIEN_MISSILE;
        }
        for (Point2D p : player2.getAlienMissiles()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.ALIEN_MISSILE;
        }
        for (Point2D p : player1.getShields()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.SHIELD;
        }
        for (Point2D p : player2.getShields()) {
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.SHIELD;
        }
        if (player1.isAlive()) {
            Point2D p = new Point2D(player1.getShipX(), 21);
            board[(int) p.getX() - 1][(int) p.getY()] = SpaceInvaderObject.SHIP;
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.SHIP;
            board[(int) p.getX() + 1][(int) p.getY()] = SpaceInvaderObject.SHIP;
        }
        if (player2.isAlive()) {
            Point2D p = new Point2D(player2.getShipX(), 1);
            board[(int) p.getX() - 1][(int) p.getY()] = SpaceInvaderObject.SHIP;
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.SHIP;
            board[(int) p.getX() + 1][(int) p.getY()] = SpaceInvaderObject.SHIP;
        }
        if (player1.isHasAlienFactory()) {
            Point2D p = new Point2D(player1.getAlienFactoryX(), 22);
            board[(int) p.getX() - 1][(int) p.getY()] = SpaceInvaderObject.ALIEN_FACTORY;
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.ALIEN_FACTORY;
            board[(int) p.getX() + 1][(int) p.getY()] = SpaceInvaderObject.ALIEN_FACTORY;
        }
        if (player2.isHasAlienFactory()) {
            Point2D p = new Point2D(player2.getAlienFactoryX(), 0);
            board[(int) p.getX() - 1][(int) p.getY()] = SpaceInvaderObject.ALIEN_FACTORY;
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.ALIEN_FACTORY;
            board[(int) p.getX() + 1][(int) p.getY()] = SpaceInvaderObject.ALIEN_FACTORY;
        }

        if (player1.isHasMissileFactory()) {
            Point2D p = new Point2D(player1.getMissileFactoryX(), 22);
            board[(int) p.getX() - 1][(int) p.getY()] = SpaceInvaderObject.MISSILE_FACTORY;
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.MISSILE_FACTORY;
            board[(int) p.getX() + 1][(int) p.getY()] = SpaceInvaderObject.MISSILE_FACTORY;
        }
        if (player2.isHasMissileFactory()) {
            Point2D p = new Point2D(player2.getMissileFactoryX(), 0);
            board[(int) p.getX() - 1][(int) p.getY()] = SpaceInvaderObject.MISSILE_FACTORY;
            board[(int) p.getX()][(int) p.getY()] = SpaceInvaderObject.MISSILE_FACTORY;
            board[(int) p.getX() + 1][(int) p.getY()] = SpaceInvaderObject.MISSILE_FACTORY;
        }

    }

    /**
     * Only check what aliens have died.
     *
     * @param p1
     * @param p2
     */
    public void collissionCheckAfterSpawn(Player p1, Player p2) {
        LinkedList<Point2D> collissions = getCollissions(1, p1.getAliens(), p1.getMissiles());
        p1.kills += collissions.size();
        p1.getAliens().removeAll(collissions);
        p1.getMissiles().removeAll(collissions);
        LinkedList<Point2D> col2 = getCollissions(1, p1.getAliens(), p2.getMissiles());
        p1.getAliens().removeAll(col2);
        p2.getMissiles().removeAll(collissions);
    }

    public LinkedList<Point2D> collissionCheckAfterMissiles(Player p1, Player p2) {
        Iterator<Point2D> it = p1.getMissiles().iterator();
        while (it.hasNext()) {
            Point2D next = it.next();
            if (next.getY() > 22 || next.getY() < 0) {
                it.remove();
            }
        }
        LinkedList<Point2D> col1 = getCollissions(1, p1.getAliens(), p1.getMissiles());
        p1.getAliens().removeAll(col1);
        p1.kills += col1.size();

        LinkedList<Point2D> col2 = getCollissions(1, p1.getAliens(), p2.getMissiles());
        p1.getAliens().removeAll(col2);

        LinkedList<Point2D> col3 = getCollissions(1, p2.getAlienMissiles(), p1.getMissiles());
        p2.getAlienMissiles().removeAll(col3);

        LinkedList<Point2D> col4 = getCollissions(1, p2.getShields(), p1.getMissiles());

        LinkedList<Point2D> col5 = getCollissions(1, p1.getShields(), p1.getMissiles());

        LinkedList<Point2D> col6 = getCollissions(1, p1.getAlienMissiles(), p1.getMissiles());
        p1.getAlienMissiles().removeAll(col6);

        p2.getShields().removeAll(col4);
        p1.getShields().removeAll(col5);

        if (p2.getShipX() > 0) {
            int y;
            if (p2.side == -1) {
                y = 1;
            } else {
                y = 21;
            }
            for (Point2D missile : p1.getMissiles()) {
                if (missile.getY() == y && Math.abs(missile.getX() - p2.getShipX()) <= 1) {
                    p2.setShipX(0);
                    p2.respawnTime = 3;
                    p1.kills++;
                    col1.add(missile);
                    break; //only 1 missile can hit a ship at a time
                }
            }
        }

        if (p2.hasAlienFactory) {
            int y;
            if (p2.side == -1) {
                y = 0;
            } else {
                y = 22;
            }
            for (Point2D missile : p1.getMissiles()) {
                if (p2.getAlienFactoryX() == y && Math.abs(missile.getX() - p2.getAlienFactoryX()) <= 1) {
                    p2.setAlienFactoryX(0);
                    p2.setHasAlienFactory(false);
                    p1.kills++;
                    col1.add(missile);
                    break; //only 1 missile can hit an alien factory at a time
                }
            }
        }

        if (p2.hasMissileFactory) {
            int y;
            if (p2.side == -1) {
                y = 0;
            } else {
                y = 22;
            }
            for (Point2D missile : p1.getMissiles()) {
                if (p2.getMissileFactoryX() == y && Math.abs(missile.getX() - p2.getMissileFactoryX()) <= 1) {
                    p2.setAlienFactoryX(0);
                    p2.setHasMissileFactory(false);
                    p1.kills++;
                    col1.add(missile);
                    break; //only 1 missile can hit an alien factory at a time
                }
            }
        }

        col1.addAll(col2);
        col1.addAll(col3);
        col1.addAll(col4);
        col1.addAll(col5);
        col1.addAll(col6);
        return col1;
    }

    public void removeOverlapping(Player p1, Player p2) {

    }

    public void collissionCheckAfterBullets(Player p1, Player p2) {
        Iterator<Point2D> it = p1.getAlienMissiles().iterator();
        while (it.hasNext()) {
            Point2D next = it.next();
            if (next.getY() > 22 || next.getY() < 0) {
                it.remove();
            }
        }
        LinkedList<Point2D> col1 = getCollissions(1, p1.getAliens(), p1.getAlienMissiles());
        p1.getAliens().removeAll(col1);
        p1.getAlienMissiles().removeAll(col1);

        LinkedList<Point2D> col2 = getCollissions(1, p1.getMissiles(), p1.getAlienMissiles());
        p1.getMissiles().removeAll(col2);
        p1.getAlienMissiles().removeAll(col2);

        LinkedList<Point2D> col3 = getCollissions(1, p1.getShields(), p1.getAlienMissiles());
        p1.getShields().removeAll(col3);
        p1.getAlienMissiles().removeAll(col3);

        if (p1.getShipX() > 0) {
            int y;
            if (p1.side == -1) {
                y = 1;
            } else {
                y = 21;
            }
            boolean killed = false;
            Iterator<Point2D> bulletIt = p1.getAlienMissiles().iterator();
            while (bulletIt.hasNext()) {
                Point2D missile = bulletIt.next();
                if (missile.getY() == y && Math.abs(missile.getX() - p1.getShipX()) <= 1) {
                    killed = true;

                    bulletIt.remove();
                }

            }
            if (killed) {
                p1.setShipX(0);
                p1.respawnTime = 3;
            }
        }

        if (p1.hasAlienFactory) {
            int y;
            if (p1.side == -1) {
                y = 0;
            } else {
                y = 22;
            }
            boolean killed = false;
            Iterator<Point2D> bulletIt = p1.getAlienMissiles().iterator();
            while (bulletIt.hasNext()) {
                Point2D missile = bulletIt.next();
                if (missile.getY() == y && Math.abs(missile.getX() - p1.getAlienFactoryX()) <= 1) {
                    killed = true;
                    bulletIt.remove();
                }

            }
            if (killed) {
                p1.setAlienFactoryX(0);
                p1.setHasAlienFactory(false);
            }
        }
        if (p1.hasMissileFactory) {
            int y;
            if (p1.side == -1) {
                y = 0;
            } else {
                y = 22;
            }
            boolean killed = false;
            Iterator<Point2D> bulletIt = p1.getAlienMissiles().iterator();
            while (bulletIt.hasNext()) {
                Point2D missile = bulletIt.next();
                if (missile.getY() == y && Math.abs(missile.getX() - p1.getMissileFactoryX()) <= 1) {
                    killed = true;
                    bulletIt.remove();
                }

            }
            if (killed) {
                p1.setMissileFactoryX(0);
                p1.setHasMissileFactory(false);
            }
        }

    }

    public void collissionCheckAfterAliens(Player p1, Player p2) {
        Iterator<Point2D> it = p1.getAliens().iterator();
        while (it.hasNext()) {
            Point2D next = it.next();
            if (next.getY() > 22 || next.getY() < 0) {
                p1.lives = 0;
                p1.respawnTime = 3;
                p1.shipX = 0;
                it.remove();
            }
        }
        LinkedList<Point2D> col1 = getCollissions(1, p1.getAliens(), p1.getMissiles());
        p1.kills += col1.size();
        p1.getAliens().removeAll(col1);
        p1.getMissiles().removeAll(col1);

        LinkedList<Point2D> col2 = getCollissions(1, p1.getAliens(), p2.getMissiles());
        p1.getAliens().removeAll(col2);
        p2.getMissiles().removeAll(col2);

        LinkedList<Point2D> col3 = getCollissions(1, p1.getAliens(), p1.getAlienMissiles());
        p1.getAliens().removeAll(col3);
        p1.getAlienMissiles().removeAll(col3);

        LinkedList<Point2D> col4 = getCollissions(1, p1.getAliens(), p1.getShields());
        p1.getAliens().removeAll(col4);
        p1.getShields().removeAll(col4);

        LinkedList<Point2D> explosions = new LinkedList<>();
        for (Point2D exp : col4) {
            for (Point2D c : cardinal) {
                Point2D explosion = new Point2D(exp.getX() + c.getX(), exp.getY() + c.getY());
                if (explosion.getX() >= 0 && explosion.getX() <= 16) {
                    if (explosion.getY() >= 0 && explosion.getY() <= 22) {
                        explosions.add(explosion);
                    }
                }
            }
        }
        boolean killed = executeShipCollissions(p1.getAliens(), p1);
        killed = executeShipCollissions(explosions, p1) | killed;
        if (killed) {
            p1.shipX = 0;
            p1.respawnTime = 3;
        }
        killed = executeAlienFactoryCollissions(p1.getAliens(), p1);
        killed = executeAlienFactoryCollissions(explosions, p1) | killed;
        if (killed) {
            p1.alienFactoryX = 0;
            p1.hasAlienFactory = false;
        }
        killed = executeMissileFactoryCollissions(p1.getAliens(), p1);
        killed = executeMissileFactoryCollissions(explosions, p1) | killed;
        if (killed) {
            p1.missileFactoryX = 0;
            p1.hasMissileFactory = false;
        }
        p1.getAliens().removeAll(explosions);
        p1.getAlienMissiles().removeAll(explosions);
        p1.getMissiles().removeAll(explosions);
        p1.getShields().removeAll(explosions);
        p2.getMissiles().removeAll(explosions);

    }

    public void postRespawnCollissionCheck(Player p1, Player p2) {
        executeShipBuildingCollissions(p1, p2);
    }

    public void postMoveCollissionCheck(Player p1, Player p2, Move p1Move) {
        if (p1Move == Move.RIGHT || p1Move == Move.LEFT) {
            postRespawnCollissionCheck(p1, p2);
        } else if (p1Move == Move.BUILD_SHIELDS) {
            //shield collisions blow themselves up too
            //LinkedList<Point2D> col1 = getCollissions(1, p1.getAliens(), p1.getShields());
            //p1.getAliens().removeAll(col1);
            LinkedList<Point2D> col2 = getCollissions(1, p1.getAlienMissiles(), p1.getShields());
            p1.getAlienMissiles().removeAll(col2);
            p1.getShields().removeAll(col2);
            LinkedList<Point2D> col3 = getCollissions(1, p1.getMissiles(), p1.getShields());
            p1.getMissiles().removeAll(col3);
            p1.getShields().removeAll(col3);
            LinkedList<Point2D> col4 = getCollissions(1, p2.getMissiles(), p1.getShields());
            p2.getMissiles().removeAll(col4);
            p1.getShields().removeAll(col4);
            collissionCheckAfterAliens(p1, p2);
        } else if (p1Move == Move.FIRE) {
            LinkedList<Point2D> collissions = this.collissionCheckAfterMissiles(p1, p2);
            p1.getMissiles().removeAll(collissions);
        } else if (p1Move == Move.BUILD_ALIEN_FACTORY) {
            executeShipBuildingCollissions(p1, p2);
        } else if (p1Move == Move.BUILD_MISSILE_FACTORY) {
            executeShipBuildingCollissions(p1, p2);
        }
    }

    /**
     * Buildings can collide with multiple things at a time. Resolve them all
     * before removing the building.
     *
     * @param p1
     * @param p2
     */
    public void executeShipBuildingCollissions(Player p1, Player p2) {
        boolean killed = false;
        killed = executeShipCollissions(p1.getAliens(), p1) | killed;
        killed = executeShipCollissions(p1.getAlienMissiles(), p1) | killed;
        int p2Missiles = p2.getMissiles().size();
        killed = executeShipCollissions(p2.getMissiles(), p1) | killed;
        if (p2Missiles > p2.getMissiles().size()) {
            p2.kills++;
        }
        if (killed) {
            p1.setShipX(0);
            p1.respawnTime = 3;
        }
        killed = false;

        killed = executeMissileFactoryCollissions(p1.getAliens(), p1) | killed;
        killed = executeMissileFactoryCollissions(p1.getAlienMissiles(), p1) | killed;
        p2Missiles = p2.getMissiles().size();
        killed = executeMissileFactoryCollissions(p2.getMissiles(), p1) | killed;
        if (p2Missiles > p2.getMissiles().size()) {
            p2.kills++;
        }
        if (killed) {
            p1.setMissileFactoryX(0);
            p1.setHasMissileFactory(false);
        }
        killed = false;

        killed = executeAlienFactoryCollissions(p1.getAliens(), p1) | killed;
        killed = executeAlienFactoryCollissions(p1.getAlienMissiles(), p1) | killed;
        p2Missiles = p2.getMissiles().size();
        killed = executeAlienFactoryCollissions(p2.getMissiles(), p1) | killed;
        if (p2Missiles > p2.getMissiles().size()) {
            p2.kills++;
        }
        if (killed) {
            p1.setAlienFactoryX(0);
            p1.setHasAlienFactory(false);
        }
    }

    public boolean executeShipCollissions(LinkedList<Point2D> objects, Player p) {
        if (p.getShipX() > 0) {
            int y;
            if (p.side == -1) {
                y = 1;
            } else {
                y = 21;
            }
            boolean killed = false;
            Iterator<Point2D> bulletIt = objects.iterator();
            while (bulletIt.hasNext()) {
                Point2D missile = bulletIt.next();
                if (missile.getY() == y && Math.abs(missile.getX() - p.getShipX()) <= 1) {
                    killed = true;

                    bulletIt.remove();
                }

            }
            return killed;
        }
        return false;
    }

    public boolean executeMissileFactoryCollissions(LinkedList<Point2D> objects, Player p) {
        if (p.hasMissileFactory) {
            int y;
            if (p.side == -1) {
                y = 0;
            } else {
                y = 22;
            }
            boolean killed = false;
            Iterator<Point2D> bulletIt = objects.iterator();
            while (bulletIt.hasNext()) {
                Point2D missile = bulletIt.next();
                if (missile.getY() == y && Math.abs(missile.getX() - p.getMissileFactoryX()) <= 1) {
                    killed = true;
                    bulletIt.remove();
                }

            }
            return killed;
        }
        return false;
    }

    public boolean executeAlienFactoryCollissions(LinkedList<Point2D> objects, Player p) {
        if (p.hasAlienFactory) {
            int y;
            if (p.side == -1) {
                y = 0;
            } else {
                y = 22;
            }
            boolean killed = false;
            Iterator<Point2D> bulletIt = objects.iterator();
            while (bulletIt.hasNext()) {
                Point2D missile = bulletIt.next();
                if (missile.getY() == y && Math.abs(missile.getX() - p.getAlienFactoryX()) <= 1) {
                    killed = true;
                    bulletIt.remove();
                }

            }
            return killed;
        }
        return false;
    }

    public boolean buildingAreaFree(Player p1, Player p2) {
        int y = 0;
        if (p1.side == -1) {
            y = 0;
        } else {
            y = 22;
        }
        Point2D building = new Point2D(p1.shipX, y);
        LinkedList<Point2D> buildings = new LinkedList<>();
        buildings.add(building);
        LinkedList<Point2D> collissions = getCollissions(3, buildings, p1.getAliens());
        if (collissions.size() > 0) {
            return false;
        }
        collissions = getCollissions(3, buildings, p1.getAlienMissiles());
        if (collissions.size() > 0) {
            return false;
        }
        collissions = getCollissions(3, buildings, p2.getMissiles());
        if (collissions.size() > 0) {
            return false;
        }
        if (p1.hasAlienFactory) {
            if (Math.abs(p1.alienFactoryX - p1.shipX) < 3) {
                return false;
            }
        }
        if (p1.hasMissileFactory) {
            if (Math.abs(p1.missileFactoryX - p1.shipX) < 3) {
                return false;
            }
        }
        return true;
    }

    /**
     * Finds all points of collission. Lists must be sorted by Y then X
     * coordinate
     *
     * @param unitWidth
     * @param largeObjects
     * @param smallObjects
     * @return
     */
    public LinkedList<Point2D> getCollissions(int unitWidth, LinkedList<Point2D> largeObjects, LinkedList<Point2D> smallObjects) {
        LinkedList<Point2D> collissions = new LinkedList<>();
        if (largeObjects.isEmpty() || smallObjects.isEmpty()) {
            return collissions;
        }

        Iterator<Point2D> large = largeObjects.iterator();
        Iterator<Point2D> small = smallObjects.iterator();

        Point2D largeCurr = large.next();
        Point2D smallCurr = small.next();

        while (true) {
            if (smallCurr.getY() < largeCurr.getY()) {
                if (small.hasNext()) {
                    smallCurr = small.next();
                } else {
                    return collissions;
                }
            } else if (smallCurr.getY() > largeCurr.getY()) {
                if (large.hasNext()) {
                    largeCurr = large.next();
                } else {
                    return collissions;
                }
            } else { //they're in the same row
                if (Math.abs(largeCurr.getX() - smallCurr.getX()) <= unitWidth / 2) {
                    if (largeCurr.equals(smallCurr)) {
                        collissions.add(smallCurr);
                    } else {
                        collissions.add(smallCurr);
                        collissions.add(largeCurr);
                    }
                    if (small.hasNext()) {
                        smallCurr = small.next();
                    } else {
                        return collissions;
                    }
                } else if (smallCurr.getX() < largeCurr.getX()) {
                    if (small.hasNext()) {
                        smallCurr = small.next();
                    } else {
                        return collissions;
                    }
                } else {
                    if (large.hasNext()) {
                        largeCurr = large.next();
                    } else {
                        return collissions;
                    }
                }

            }
        }
    }

    @Override
    public Point2D getPlayer1AlienPrimaryBullet() {
        return this.primaryBullet1;
    }

    @Override
    public Point2D getPlayer2AlienPrimaryBullet() {
        return this.primaryBullet2;
    }

    @Override
    public List<Point2D> getPlayer1AlienSecondaryBullets() {
        return this.secondaryBullet1;
    }

    @Override
    public List<Point2D> getPlayer2AlienSecondaryBullets() {
        return this.secondaryBullet2;
    }

    public ArrayList<MoveValue> getUniqueMovesP1() {
        if (moveValues != null) {
            return moveValues;
        }
        moveValues = new ArrayList<>();
        Move p2Move = Move.NONE;
        List<Point2D> bullets = getPlayer1AlienSecondaryBullets();
        Point2D bullet = getPlayer1AlienPrimaryBullet();
        LinkedList<GameState> seenStates = new LinkedList<>();
        for (Move m : moves) {
            MoveValue mv = new MoveValue(Double.NEGATIVE_INFINITY, m);
            mv.children = new ArrayList<>();
            if (bullet != null) {
                GameState g = this.getNextState(m, p2Move, bullet);
                //if (!seenStates.contains(g)) {
                seenStates.add(g);
                mv.children.add(new AlienMove(g, bullet, Double.POSITIVE_INFINITY, 1.0 / 3.0));
                //}
                if (bullets.isEmpty()) {
                    g = this.getNextState(m, p2Move);
                    //if (!seenStates.contains(g)) {
                    seenStates.add(g);
                    mv.children.add(new AlienMove(g, null, Double.POSITIVE_INFINITY, 2.0 / 3.0));
                    //}
                }
            } else {
                GameState g = this.getNextState(m, p2Move);
                //if (!seenStates.contains(g)) {
                seenStates.add(g);
                mv.children.add(new AlienMove(g, bullet, Double.POSITIVE_INFINITY, 1.0));
                //}
            }
            for (Point2D b : bullets) {
                GameState g = this.getNextState(m, p2Move, b);
                //if (!seenStates.contains(g)) {
                seenStates.add(g);
                mv.children.add(new AlienMove(g, b, Double.POSITIVE_INFINITY, 2.0 / (3.0 * bullets.size())));
                //}
            }

            if (!mv.children.isEmpty()) {
                moveValues.add(mv);
            }
        }
        return moveValues;
    }

    public boolean player1Lost() {
        return (player1.lives == 0 && player1.getShipX() == 0);
    }

    public int getPlayer1AlienWaveWidth() {
        if (player1.rightmostAlienX - player1.leftmostAlienX >= 0) {
            return (player1.rightmostAlienX - player1.leftmostAlienX) + 1;
        } else {
            return 0;
        }
    }

    public int getPlayer1AlienWaveHeight() {
        return player1.maxCentreDistance;
    }

    public int getPlayer2AlienWaveHeight() {
        return player2.maxCentreDistance;
    }

    public int getPlayer1AlienBottomRowSize() {
        return player1.bottomRowAliens;
    }

    //will always be less than or equal to height
    public double getPlayer1AlienWaveStrength() {
        double strength = (player1.leftStrength + player1.rightStrength) / 2.0;
        return strength;
    }

    public double getAliensOnFontierEdge() {
        if (player1.delta == -1) {
            return player1.leftStrength;
        } else {
            return player1.rightStrength;
        }
    }

    //aliens are weighted according to their manhattan distance to the closest side and the back. The back edge aliens weigh the most.
    public double getPlayer1AlienWeights() {
        double weight = 0;
        int x;
        int x2;
        if (getPlayer1SpawnSide() == 0) {
            x = player1.leftmostAlienX; //the further away from this x, the worse
            x2 = player1.rightmostAlienX;
        } else {
            x = player1.rightmostAlienX;
            x2 = player1.leftmostAlienX;
        }
        if (player1.getAliens().size() > 10 || (getPlayer1AlienWaveHeight() >= 5 && getPlayer1AlienWaveWidth() >= 9) || getPlayer1AlienWaveHeight() >= 7) {

            for (Point2D alien : player1.getAliens()) {
                weight += 1;
                weight += (double) Math.abs(alien.getY() - 11);
                weight += 2 * (Math.abs(alien.getX() - x));
                //weight += Math.abs(((getPlayer1AlienWaveWidth() / 2.0) - Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))));
                //weight += Math.pow(alien.getY() -  18,2)/36.0;
                //weight += Math.pow(((getPlayer1AlienWaveWidth()/2.0) - Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))),2)/49.0;
            }
        } else {

            for (Point2D alien : player1.getAliens()) {
                weight += (Math.abs(alien.getX() - x2));
                weight += Math.abs(alien.getY() - 15);
                weight += 1;
                if (alien.getX() == this.player1.leftmostAlienX) {
                    weight -= 0.5;
                }
                if (alien.getX() == this.player1.rightmostAlienX) {
                    weight -= 0.5;
                }
                //weight -= 0.2*Math.abs(alien.getX() - x2) + 0.2*(Math.abs(alien.getX() - x));
                //weight += 0.3*Math.abs(alien.getX() - x2) + 0.3*(Math.abs(alien.getX() - x)) + 1;
                weight += 0.3 * Math.abs((Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))));
                //weight += Math.abs(((getPlayer1AlienWaveWidth() / 2.0) - Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))));
                //weight += Math.pow(alien.getY() -  18,2)/36.0;
                //weight += Math.pow(((getPlayer1AlienWaveWidth()/2.0) - Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))),2)/49.0;
            }
        }
        return weight;
    }

    public double getPlayer1OldAlienWeights() {
        double rightMod = 1;
        double leftMod = 1;
        /*
        if (player1MissileFactory()) {
            if (getPlayer1MissileFactory() > 8) {
                leftMod = 1.5;
                rightMod = 0.8;
            }
            else {
                leftMod = 0.8;
                rightMod = 1.5;
            }
        }
        */
        double weight = 0;
        //if (player1.getAliens().size() > 8 || (getPlayer1AlienWaveHeight() >= 5 && getPlayer1AlienWaveWidth() >= 9) || getPlayer1AlienWaveHeight() >= 7) {
        for (Point2D alien : player1.getAliens()) {
            weight += Math.max(leftMod*Math.abs(alien.getX() - player1.leftmostAlienX), rightMod*Math.abs(alien.getX() - player1.rightmostAlienX)) / 15.0;
            //weight += Math.abs(((getPlayer1AlienWaveWidth() / 2.0) - Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))));
            weight += Math.pow(alien.getY() - 16, 2) / 25.0;
            //weight += Math.pow(((getPlayer1AlienWaveWidth()/2.0) - Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX))),2)/49.0;
        }
        //} else {
        //    for (Point2D alien : player1.getAliens()) {
        //        weight += (Math.min(Math.abs(alien.getX() - player1.leftmostAlienX), Math.abs(alien.getX() - player1.rightmostAlienX)));
        //        weight += Math.pow(alien.getY() - 18, 2) / 36.0;
        //    }
        //}
        return weight;

    }

    public double distanceToAliens() {
        double weight = 0;
        for (Point2D alien : player1.getAliens()) {
            weight += Math.abs((alien.getX() - getPlayer1X()) / 3.0);
        }

        return weight / (player1.getAliens().size() + 1);
    }

    public double heightWeight() {
        double weight = 0;
        for (Point2D alien : player1.getAliens()) {
            weight += (alien.getY() - 12) / 2.0;
        }

        return weight;
    }

    public double closeAlienWeights() {
        double weight = 0;
        for (Point2D alien : player1.getAliens()) {
            if ((int) alien.getY() == 17) {
                weight += 0.21;
            } else if ((int) alien.getY() == 18) {
                weight += 0.5;
            } else if ((int) alien.getY() == 19) {
                weight += 1.5;
            } else if (((int) alien.getY() == 20)) {
                weight += 9;
            } else if (((int) alien.getY() == 21)) {
                weight += 35;
            } else if ((int) alien.getY() == 22) {
                weight += 70;
            }
        }

        return weight;
    }

    public double getExtremeWeights() {
        double weight = 0;
        int x;
        int x2;
        if (getPlayer1SpawnSide() == 0) {
            x = player1.leftmostAlienX; //the further away from this x, the worse
            x2 = player1.rightmostAlienX;
        } else {
            x = player1.rightmostAlienX;
            x2 = player1.leftmostAlienX;
        }
        for (Point2D alien : player1.getAliens()) {
            double w = (Math.abs(alien.getX() - x));
            if (w == 12) {
                weight += 6;
            }
            if (w == 9) {
                weight += 2;
            }
            if (w == 6) {
                weight += 0.3;
            }
            if (w == 3) {
                weight += 0.2;
            }
            if (w == 0) {
                weight = 0.1;
            }
            if (alien.getY() > 18) {
                weight += 6;
            } else if (alien.getY() == 18.0) {
                weight += 2;
            }
        }
        return weight / 30.0;
    }

    public int getPlayer1Player1SpawnTime() {
        if (player1.getAliens().isEmpty()) {
            return 0;
        }
        if (player1.delta == -1) {
            if (getPlayer1SpawnSide() == 0) {
                return player1.leftmostAlienX + 1;
            } else {
                if (player1.rightmostAlienX == 16 && getYOffset() == 0) {
                    return 0;
                }
                return 2 * player1.leftmostAlienX + (16 - player1.rightmostAlienX) + 2;
            }
        } else {
            if (getPlayer1SpawnSide() == 16) {
                return (16 - player1.rightmostAlienX) + 1;
            } else {
                if (player1.leftmostAlienX == 0 && getYOffset() == 0) {
                    return 0;
                }
                return 2 * (16 - player1.rightmostAlienX) + player1.leftmostAlienX + 2;
            }
        }
    }

    public int getPlayer1SpawnSide() {
        if (player1.centreDistance % 2 == 0) {
            if (player1.delta == -1) {
                return 0;
            } else {
                return 16;
            }
        } else {
            if (player1.delta == -1) {
                return 16;
            } else {
                return 0;
            }
        }
    }

    public int getPlayer2SpawnSide() {
        if (player2.centreDistance % 2 == 0) {
            if (player2.delta == -1) {
                return 0;
            } else {
                return 16;
            }
        } else {
            if (player2.delta == -1) {
                return 16;
            } else {
                return 0;
            }
        }
    }

    public void setPlayer1ShotEnergy(int shotEnergy) {
        player1.shotEnergy = shotEnergy;
    }

    public boolean getFlip() {
        //if (player1.delta == -1) {
        if (getPlayer1SpawnSide() == 16) {
            return false;
        }
        return true;
    }

    public int getFlippedXOffset() {
        int x;
        boolean flip = false;
        //if (player1.delta == -1) {
        if (getPlayer1SpawnSide() == 16) {
            x = player1.leftmostAlienX;
            while (x > 4) {
                x -= 3;
            }
        } else {
            x = player1.rightmostAlienX;
            while (x < 12) {
                x += 3;
            }
            x = 16 - x;
            flip = true;
        }
        return x;
    }

    public int getYOffset() {
        return player1.centreDistance % 2;
    }

    public double[] getAlienBinaryFeatures() {
        ArrayList<Double> values = new ArrayList<>();

        int x = getFlippedXOffset();
        boolean flip = getFlip();

        int yOffset = getYOffset();

        int numInRows[] = getRowSizes(x, yOffset, flip);
        int numInCols[] = getColSizes(x, yOffset, flip);

        for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
            for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                for (int row = 0; row < 5; row++) {
                    for (int numInRow = 0; numInRow < 6; numInRow++) {
                        if (xoffsets == x && yOffset == yoffsets && numInRows[row] == numInRow) {
                            values.add(1.0);
                        } else {
                            values.add(0.0);
                        }
                    }
                }
            }
        }
        for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
            for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                for (int col = 0; col < 5; col++) {
                    for (int numInCol = 0; numInCol < 6; numInCol++) {
                        if (xoffsets == x && yOffset == yoffsets && numInCols[col] == numInCol) {
                            values.add(1.0);
                        } else {
                            values.add(0.0);
                        }
                    }
                }
            }
        }
        //values.addAll(getBackRowFeatures());
        double d[] = new double[values.size()];
        int i = 0;
        for (Double doub : values) {
            d[i] = doub;
            i++;
        }
        return d;
    }

    public List<Double> getBackRowFeatures() {
        ArrayList<Double> values = new ArrayList<>();
        boolean record = false;
        if (player1.maxCentreDistance < 3) {
            record = true;
        }
        int xOffset = getFlippedXOffset();
        boolean flip = getFlip();

        int yOffset = getYOffset();

        int y = yOffset + 11;

        int count = 0;
        for (int i = 0; i < 5; i++) {
            int x = xOffset + i * 3;
            if (flip) {
                x = 16 - x;
            }
            if (this.getSpaceInvaderObjectAt(new Point2D(x, y)) == SpaceInvaderObject.ALIEN) {
                count += Math.pow(2, i);
            }
        }
        for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
            for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                for (int col = 0; col < 5; col++) {
                    for (int i = 0; i < 32; i++) {
                        if (xoffsets == xOffset && yOffset == yoffsets && count == i && record) {
                            values.add(1.0);
                        } else {
                            values.add(0.0);
                        }
                    }
                }
            }
        }
        return values;
    }

    public int getColSize(int x, int yOffset) {
        int count = 0;
        for (int j = 0; j < 5; j++) {
            int y = j * 2 + yOffset + 11;

            if (this.getSpaceInvaderObjectAt(new Point2D(x, y)) == SpaceInvaderObject.ALIEN) {
                count++;
            }
        }
        return count;
    }

    public int[] getColSizes(int xOffset, int yOffset, boolean flip) {
        int colSizes[] = new int[5];
        for (int i = 0; i < 5; i++) {
            int x = xOffset + i * 3;
            if (flip) {
                x = 16 - x;
            }
            int count = 0;
            for (int j = 0; j < 5; j++) {
                int y = j * 2 + yOffset + 11;

                if (this.getSpaceInvaderObjectAt(new Point2D(x, y)) == SpaceInvaderObject.ALIEN) {
                    count++;
                }
            }
            if (count > 5) {
                count = 5;
            }
            colSizes[i] = count;
        }

        return colSizes;
    }

    public int[] getRowSizes(int xOffset, int yOffset, boolean flip) {
        int rowSizes[] = new int[5];
        for (int j = 0; j < 5; j++) {
            int y = j * 2 + yOffset + 11;
            int count = 0;
            for (int i = 0; i < 5; i++) {
                int x = xOffset + i * 3;
                if (flip) {
                    x = 16 - x;
                }
                if (this.getSpaceInvaderObjectAt(new Point2D(x, y)) == SpaceInvaderObject.ALIEN) {
                    count++;
                }
            }
            if (count > 5) {
                count = 5;
            }
            rowSizes[j] = count;

        }

        return rowSizes;
    }

    /*
     public double[] getState() {
     boolean flip = false;
     double[] state = new double[10];
     state[0] = player1.lives;
     if (player1.delta == -1) {
     flip = false;
     int x = player1.leftmostAlienX;
     while (x >= 4) {
     x -= 3;
     }
     state[1] = x;
     for (int i = 0; i < 5; i++) {
     Point2D alien = player1.getAlienInColumn(x + i * 3);
     if (alien != null) {
     state[i + 2] = 22 - alien.getY();
     } else {
     state[i + 2] = 22;
     }
     }
     } else {
     flip = true;
     int x = player1.rightmostAlienX;
     while (x <= 12) {
     x += 3;
     }
     state[1] = x;
     for (int i = 0; i < 5; i++) {
     Point2D alien = player1.getAlienInColumn(x - i * 3);
     if (alien != null) {
     state[i + 2] = 22 - alien.getY();
     } else {
     state[i + 2] = 22;
     }
     }
     }

     if (!flip) {
     state[6] = player1.shipX;
     } else {
     if (player1.shipX == 0) {
     state[6] = 0;
     } else {
     state[6] = 16 - player1.shipX;
     }
     }
     state[7] = player1.shipX - player1.missileFactoryX;
     state[8] = player1.shipX - player1.alienFactoryX;
     return state;
     }

     public double[] getStateMinValues() {

     }

     public double[] getStateMaxValues() {

     }
     */
    public double getReward(GameState previous) {
        return 0;
    }
}
