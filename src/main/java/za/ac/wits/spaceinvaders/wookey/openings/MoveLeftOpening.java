
package za.ac.wits.spaceinvaders.wookey.openings;

import za.ac.wits.spaceinvaders.wookey.Opening;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class MoveLeftOpening extends Opening {

    @Override
    public Move[] getOpeningMoves() {
        return new Move[]{Move.LEFT,Move.LEFT,Move.LEFT,Move.LEFT,Move.LEFT,Move.LEFT,Move.BUILD_MISSILE_FACTORY,Move.FIRE,Move.FIRE};
    }
    
}
