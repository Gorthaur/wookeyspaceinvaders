package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public class AlienMissileFactoryFeature extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 4;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        if (g.player2AlienFactory()) {
            if (g.player1MissileFactory()) {
                return new double[]{1, 0, 1, 0};
            } else {
                return new double[]{1, 0, 0, 1};
            }
        } else {
            if (g.player1MissileFactory()) {
                return new double[]{0, 1, 1, 0};
            } else {
                return new double[]{0, 1, 0, 1};
            }
        }

    }

    @Override
    public int[] getActiveFeatures(GameState g) {
        if (g.player2AlienFactory()) {
            if (g.player1MissileFactory()) {
                return new int[]{0, 2};
            } else {
                return new int[]{0, 3};
            }
        } else {
            if (g.player1MissileFactory()) {
                return new int[]{1, 2};
            } else {
                return new int[]{1, 3};
            }
        }
    }

}
