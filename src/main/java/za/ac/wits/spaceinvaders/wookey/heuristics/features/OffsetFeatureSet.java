package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class OffsetFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 10;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        double values[] = new double[10];
        int x = g.getFlippedXOffset();

        int yOffset = g.getYOffset();
        int count = 0;
        for (int xoffsets = 0; xoffsets < 5; xoffsets++) {
            for (int yoffsets = 0; yoffsets < 2; yoffsets++) {
                if (xoffsets == x && yOffset == yoffsets) {
                    values[count] = 1.0;
                }
                count++;
            }
        }
        return values;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int x = g.getFlippedXOffset();

        int yOffset = g.getYOffset();
        int maxValues[] = new int[]{5, 2};
        int index = getIndex(maxValues, new int[]{x, yOffset});
        return new int[]{index};
    }

}
