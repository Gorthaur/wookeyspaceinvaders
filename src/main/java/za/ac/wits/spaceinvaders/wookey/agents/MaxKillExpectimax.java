package za.ac.wits.spaceinvaders.wookey.agents;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.Agent;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.PlayerMove;
import za.ac.wits.spaceinvaders.wookey.ThreadedAgent;
import za.ac.wits.spaceinvaders.wookey.heuristics.AlienFactoryGameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.AlienKillGameState;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpening;
import za.ac.wits.spaceinvaders.wookey.openings.FireMoveRightOpeningThree;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class MaxKillExpectimax extends Expectimax implements TestableAgent {

    public MaxKillExpectimax() {
                this.setOpening(new FireMoveRightOpeningThree());
    }

    @Override
    public String getDescription() {
        return "Alien Expectimax";
    }

    @Override
    public GameState getGameState(InputStream in) throws IOException {
        return new AlienKillGameState(in);
    }
}
