
package za.ac.wits.spaceinvaders.wookey;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public abstract class ThreadedAgent extends Agent {
    protected LinkedList<Move> bestMoves = new LinkedList<>();
    private final Object bestMoveLock = new Object();
    private Thread thread;
    
    public ThreadedAgent() {
        bestMoves.add(Move.NONE);
    }
    
    @Override
    public Move getCalculatedMove(GameState state) {
        return getCalculatedMoves(state).get(0);
    }
    
        @Override
    public List<Move> getCalculatedMoves(GameState state) {
                long moveTime = getMoveTime();
        
        thread = new Thread(()->{
            try {
                threadedMoveCalculate(state);
            }
            catch (InterruptedException e) {
                Thread.interrupted();
            }
        });
        thread.setDaemon(true);
        thread.start();
        try {
            Thread.sleep(moveTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadedAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        long time = System.currentTimeMillis();
       thread.interrupt();
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadedAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bestMoves;
    }
    
    public List<Move> getBestMoves() {
        synchronized (bestMoveLock) {
            return bestMoves;
        }
    }
    
    public void setBestMoves(List<Move> moves) {
        synchronized (bestMoveLock) {
            bestMoves.clear();
            bestMoves.addAll(moves);
        }
    }

    protected abstract void threadedMoveCalculate(GameState g) throws InterruptedException;
    
    
    public boolean isInterrupted() {
        return thread.isInterrupted();
    }
    
    public void checkInterrupted() throws InterruptedException {
        if (isInterrupted()) {
            throw new InterruptedException();
        }
    }
}
