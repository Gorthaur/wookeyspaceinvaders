
package za.ac.wits.spaceinvaders.wookey.openings;

import za.ac.wits.spaceinvaders.wookey.Opening;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class FireMoveRightOpeningTwo extends Opening {

    @Override
    public Move[] getOpeningMoves() {
        return new Move[]{Move.FIRE, Move.RIGHT,Move.RIGHT,Move.RIGHT,Move.RIGHT,Move.BUILD_MISSILE_FACTORY, Move.RIGHT, Move.FIRE, Move.FIRE, Move.FIRE,Move.RIGHT,Move.FIRE,Move.FIRE, Move.FIRE};
    }
    
}
