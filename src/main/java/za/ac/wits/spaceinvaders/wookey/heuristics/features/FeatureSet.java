package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import java.util.Arrays;
import za.ac.wits.spaceinvaders.wookey.GameState;

/**
 *
 * @author Dean
 */
public abstract class FeatureSet {

    GameState last = null;
    int actives[];

    public abstract int getNumFeatures();

    public abstract double[] getFeatureValues(GameState g);

    public final int[] getActiveIndices(GameState g) {
        last = g;
        actives = getActiveFeatures(g);
        return actives;
    }

    public abstract int[] getActiveFeatures(GameState g);

    public int getIndex(int loopMaxValues[], int values[]) {
        return getIndex(0, loopMaxValues, values);
    }

    public int getIndex(int loopNum, int loopMaxValues[], int values[]) {
        if (loopNum == values.length - 1) {
            return values[loopNum];
        }
        if (values[loopNum] == 0) {
            return getIndex(loopNum + 1, loopMaxValues, values);
        }
        int index = values[loopNum];
        for (int i = loopNum + 1; i < values.length; i++) {
            index *= loopMaxValues[i];
        }
        return index + getIndex(loopNum + 1, loopMaxValues, values);
    }

    public double[] getValues(GameState g) {
        if (g == last) {
            double vals[] = new double[actives.length];
            Arrays.fill(vals, 1.0);
            return vals;
        } else {
            double vals[] = new double[getActiveIndices(g).length];
            Arrays.fill(vals, 1.0);
            return vals;
        }
    }
}
