package za.ac.wits.spaceinvaders.wookey.agents;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import za.ac.wits.spaceinvaders.wookey.Agent;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;

public class RandomAgent extends Agent {

    @Override
    public Move getCalculatedMove(GameState state) {
        int num = (int) (Math.random() * 3);
        if (num == 0) {
            return Move.FIRE;
        } else if (num == 1) {
            return Move.LEFT;
        } else {
            return Move.RIGHT;
        }
    }

    @Override
    public String getDescription() {
        return "Random";
    }

    @Override
    public List<Move> getCalculatedMoves(GameState state) {
        LinkedList<Move> bestMoves = new LinkedList<>();
        bestMoves.add(Move.NONE);
        return bestMoves;
    }

}
