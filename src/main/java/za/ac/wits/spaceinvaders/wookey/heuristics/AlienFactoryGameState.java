package za.ac.wits.spaceinvaders.wookey.heuristics;

import java.io.IOException;
import java.io.InputStream;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.witstestablespaceinvaders.Move;

/**
 *
 * @author Dean
 */
public class AlienFactoryGameState extends SimpleState {

    public AlienFactoryGameState(InputStream in) throws IOException {
        super(in);
    }

    protected AlienFactoryGameState(GameState previous, Move player1Move, Move player2Move) {
        this(previous, player1Move, player2Move, new Point2D[0]);
    }

    protected AlienFactoryGameState(GameState previous, Move player1Move, Move player2Move, Point2D[] bullets) {
        super(previous, player1Move, player2Move, bullets);
    }

    @Override
    public SimpleState getNextState(Move move, Move move1) {
        SimpleState g = new AlienFactoryGameState(this, move, move1);
        return g;
    }

    @Override
    public GameState getNextState(Move move, Move move1, Point2D... pds) {
        return new AlienFactoryGameState(this, move, move1, pds);
    }
    
    @Override
        public double calculateStateValue() {
            
        if (player1Lost()) {
            return -1000;
        }
        double value = getPlayer1Kills();
        value += -20 * missileMisses;
        value += 35 * lives;
        //value += 0.2 * currentAlienValue;
        value += -2 * dangerousBullets;
        value += 5 * futureBulletMisses;
        value += -3 * futureDangerousBullets;
        value += 40 * missileFactory;
        value += 60 * alienFactory;
        //value += (-5 * squaredDistanceFromSpawnSide) / 144.0;
        value += (-30 * squaredDistanceFromMissileFactory) / 144.0;
        //value += (-5 * unusedBullets);
        value += (-10 * superDangerousBullets);
        return value;
                    
    }
    

        /*
    @Override
    public double getStateValue() {
        if (player1Lost()) {
            //return g.getPlayer1Kills();
            return -1000;
        }
        GameState curr = this;
        double value = this.getPlayer1Kills() * 1.1; //slightly value kills already in the bag more

        boolean over = false;
        for (int i = 0; i < 10; i++) {
            int kills = curr.getPlayer1Kills();
            curr = curr.getNextState(Move.NONE, Move.NONE);
            int diff = curr.getPlayer1Kills() - kills;
            value += Math.min(diff * 1, (diff * 3.0) / (i + 1)); //favour kills that happen quicker
            for (Point2D p : curr.getPlayer1Missiles()) {

                if (p.getY() <= 11) {
                    over = true;
                }
            }
        }
        if (over) {
            value -= 25; //penalty for missing if an alien doesn't fire
        }
        value += getAlienValue(curr);
        value += this.getPlayer1Lives() * 35;

        if (this.player1MissileFactory()) {
            value += 40;
            if (this.player1ShipAlive()) {
                value -= Math.abs(this.getPlayer1X() - this.getPlayer1MissileFactory()) * 0.5;
            } else {
                value -= Math.abs(8 - this.getPlayer1MissileFactory()) * 0.5;
            }
        }

        value -= getBulletValue(this, this.getPlayer1AlienMissiles());
        value -= getBulletValue(this, this.getPlayer2Missiles());

        if (this.player1ShipAlive()) {
            value += 60;
        }
        //value -= g.getPlayer1AlienWaveHeight() * g.getPlayer1AlienWaveWidth();
        value += (this.getPlayer1MissileLimit() - this.getPlayer1Missiles().size());
        value -= this.getPlayer1Aliens().size() * 0.2;
        value += this.getPlayer2Aliens().size();
        if (this.player1AlienFactory()) {
            value += 50;
            if (this.player1ShipAlive()) {
                value -= Math.abs(this.getPlayer1X() - this.getPlayer1AlienFactory()) * 0.5;
            } else {
                value -= Math.abs(8 - this.getPlayer1AlienFactory()) * 0.5;
            }
        }
        return value;
    }
        */
    
}
