package za.ac.wits.spaceinvaders.wookey.heuristics.features;

import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;

/**
 *
 * @author Dean
 */
public class WidthHeightFeatureSet extends FeatureSet {

    @Override
    public int getNumFeatures() {
        return 35;
    }

    @Override
    public double[] getFeatureValues(GameState g) {
        double vals[] = new double[35];
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int height = (g.getPlayer1AlienWaveHeight() - 1) / 2;
        if (height < 0) {
            height = 0;
        } else {
            height = height + 1;
        }
        int width = (g.getPlayer1AlienWaveWidth() + 2) / 3;
        width = width - 1;

        int count = 0;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 5; j++) {
                if (i == height && j == width) {
                    vals[count] = 1;
                }
                count++;
            }
        }
        return vals;
    }

    @Override
    public int[] getActiveFeatures(GameState g) {
                if (g instanceof SimpleState) {
            ((SimpleState)g).initialiseStateVariables();
            g = ((SimpleState)g).next;
        }
        int height = (g.getPlayer1AlienWaveHeight() - 1) / 2;
        if (height < 0) {
            height = 0;
        } else {
            height = height + 1;
        }
        int width = (g.getPlayer1AlienWaveWidth() + 2) / 3;
        width = width - 1;
        int[] max = new int[]{7, 5};
        int index = this.getIndex(max, new int[]{height, width});
        return new int[]{index};
    }

}
