package za.ac.wits.spaceinvaders.wookey.agents;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Point2D;
import za.ac.wits.spaceinvaders.wookey.Agent;
import za.ac.wits.spaceinvaders.wookey.GameState;
import za.ac.wits.spaceinvaders.wookey.PlayerMove;
import za.ac.wits.spaceinvaders.wookey.ThreadedAgent;
import za.ac.wits.spaceinvaders.wookey.heuristics.SimpleState;
import za.ac.wits.witstestablespaceinvaders.Move;
import za.ac.wits.witstestablespaceinvaders.TestableAgent;

/**
 *
 * @author Dean
 */
public class SimpleMiniMax extends ThreadedAgent implements TestableAgent {

    public static ExecutorService executor = Executors.newFixedThreadPool(7,
            new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = Executors.defaultThreadFactory().newThread(r);
                    t.setDaemon(true);
                    return t;
                }
            });

    public SimpleMiniMax() {
    }

    @Override
    public String getDescription() {
        return "Simple MiniMax";
    }

    @Override
    public GameState getGameState(InputStream in) throws IOException {
        return new SimpleState(in);
    }

    @Override
    public void threadedMoveCalculate(GameState g) throws InterruptedException {
        //System.out.println("Spawn side " + g.getPlayer1SpawnSide());
        //System.out.println(getHeuristicValue(g));
        AlienMove root = new AlienMove(g, null, 0);
        for (int depth = 3; depth < 15; depth++) {
            checkInterrupted();
            double alpha = Double.NEGATIVE_INFINITY;
            double beta = Double.POSITIVE_INFINITY;
            alphaBeta(null, root, depth, alpha, beta, new ConcurrentHashMap<>());

            LinkedList<Move> bestMoves = new LinkedList<>();
            if (root.children == null) {
                bestMoves.add(Move.NONE);
                setBestMoves(bestMoves);
            } else {
                MoveValue bestMove = root.children.get(0);
                bestMoves.add(bestMove.move);
                while (bestMove.children.get(0).children != null) {
                    bestMove = bestMove.children.get(0).children.get(0);
                    if (bestMove.move != null) {
                        bestMoves.add(bestMove.move);
                    }
                }
                setBestMoves(bestMoves);
            }
            System.out.println("Depth " + getDescription() + " " + depth);
            //System.out.println(Arrays.toString(SimpleState.taskChooseTimes));

            //System.out.println(depth + " " + bestMove);
            /*
             for (MoveValue m : root.children) {
             MoveValue curr = m;
             while (curr.children.get(0).children != null) {
             curr = curr.children.get(0).children.get(0);
             //getHeuristicValue(curr.children.get(0).result);
             }
             System.out.println(this.getDescription());
             System.out.println(m.move);

             System.out.println(curr.children.get(0).result.getStateValue());
             System.out.println(curr.children.get(0).result.getMapString());
             }
             System.out.println(bestMove);
             */
        }
    }

    public void alphaBeta(AlienMove parentParent, AlienMove parent, int depth, double alpha, double beta, ConcurrentHashMap<GameState, Double> values) throws InterruptedException {
        final double reward = getReward(parentParent, parent);
        if (values.containsKey(parent.result)) {
            parent.value = values.get(parent.result);
            return;
        }
        checkInterrupted();
        if (depth == 0 || (parent.result.player1Lost()) || parent.result.getRound() == 200) {
            parent.value = getStateValue(parent.result, depth) + reward;
            values.put(parent.result, parent.value);
            return;
        }

        if (parent.children == null) {
            initialiseChildren(parent);
        }
        double value = Double.NEGATIVE_INFINITY;
        /*
        if (depth == 3) {
            LinkedList<Future<Double>> futures = new LinkedList<>();
            for (MoveValue mv : parent.children) {
                final MoveValue fmv = mv;
                final double fAlpha = alpha;
                final double fBeta = beta;
                futures.add(executor.submit((Callable<Double>) () -> {
                    alphaBetaAlien(parent, fmv, depth, fAlpha, fBeta, values);
                    double v = mv.value + reward;
                    return v;
                }));
            }
            for (Future<Double> f : futures) {
                double v;
                try {
                    v = f.get();
                    if (v > value) {
                        value = v;
                    }
                } catch (ExecutionException ex) {
                    Logger.getLogger(SimpleMiniMax.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                */
        //} else {
            for (MoveValue mv : parent.children) {
                alphaBetaAlien(parent, mv, depth, alpha, beta, values);
                double v = mv.value + reward;
                if (v > value) {
                    value = v;
                }
                alpha = Math.max(alpha, value);
                if (beta <= alpha) {
                    break;
                }
            }
        //}
        parent.value = value;
        values.put(parent.result, parent.value);
        Collections.sort(parent.children);
    }

    public void alphaBetaAlien(AlienMove parentParent, MoveValue parent, int depth, double alpha, double beta, ConcurrentHashMap<GameState, Double> values) throws InterruptedException {
        checkInterrupted();
        double value = Double.POSITIVE_INFINITY;
        for (AlienMove aMove : parent.children) {
            alphaBeta(parentParent, aMove, depth - 1, alpha, beta, values);
            double v = aMove.value;
            if (v < value) {
                value = v;
            }
            beta = Math.min(beta, value);
            if (beta <= alpha) {
                break;
            }
        }
        parent.value = value;
        Collections.sort(parent.children);
    }

    public double getReward(AlienMove parentParent, AlienMove parent) {
        if (parentParent == null) {
            return 0;
        } else {
            return parent.result.getReward(parentParent.result);
        }
    }

    public double getStateValue(GameState g, int depth) throws InterruptedException {
        return g.getStateValue();
    }

    public void initialiseChildren(AlienMove parent) {
        parent.children = parent.result.getUniqueMovesP1();
    }

}
